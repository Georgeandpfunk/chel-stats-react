import dimensions from './dimensions'

function populatePlayerData (data) {
    let result = []

    data.forEach(member => {
        let player = {}
        let stats = []
    
        player.name = member.name
        player.position = member.favoritePosition
        player.id = member.id 
        
        for ( const property in member ) {
            for ( let i = 0; i < dimensions.dimensions.length; i++ ) {
                if ( dimensions.dimensions[i].statName == property ) {
                                      
                    let stat = {
                        hidden: dimensions.dimensions[i].hidden,            
                        statName: dimensions.dimensions[i].statName,
                        fullName: dimensions.dimensions[i].fullName,
                        type: dimensions.dimensions[i].type,
                        catg: dimensions.dimensions[i].catg,
                        subCatg: dimensions.dimensions[i].subCatg,
                        subCatgSortOrder: dimensions.dimensions[i].subCatgSortOrder,
                        value: member[property]}
                        
                    stats.push(stat)
                }
            }
        }

        player.stats = stats
        result.push(player)
    })

    return result
}

function updateCustomStats(data) {
    let result = data
/*
    for (let i = 0; i < data.length; i++ ) {
        let obj = JSON.parse(JSON.stringify(data[i]))
        result.push(obj)
    }
*/
    for ( let i = 0; i < result.length; i++ ) {
        let gamesPlayed = parseFloat(result[i].stats.find(stat => stat.statName === 'skgp').value)
        let goalsScored = parseFloat(result[i].stats.find(stat => stat.statName === 'skgoals').value)
        let assistsScored = parseFloat(result[i].stats.find(stat => stat.statName === 'skassists').value)
        let dekesMade = parseFloat(result[i].stats.find(stat => stat.statName === 'skdekesmade').value)
        let totalDekes = parseFloat(result[i].stats.find(stat => stat.statName === 'skdekes').value)
        let hitsMade = parseFloat(result[i].stats.find(stat => stat.statName === 'skhits').value)
        let totalFights = parseFloat(result[i].stats.find(stat => stat.statName === 'skfights').value)
        let trueTotalFights = parseFloat(result[i].stats.find(stat => stat.statName === 'skfights').value)
        let fightsWon = parseFloat(result[i].stats.find(stat => stat.statName === 'skfightswon').value)
        let totalGiveaways = parseFloat(result[i].stats.find(stat => stat.statName === 'skgiveaways').value)
        let totalOffsides = parseFloat(result[i].stats.find(stat => stat.statName === 'skoffsides').value)
        let totalPenMins = parseFloat(result[i].stats.find(stat => stat.statName === 'skpim').value)
        let totalPoints = parseFloat(result[i].stats.find(stat => stat.statName === 'skpoints').value)
        let totalShots = parseFloat(result[i].stats.find(stat => stat.statName === 'skshots').value)
        let totalTakeaways = parseFloat(result[i].stats.find(stat => stat.statName === 'sktakeaways').value)
        let deflections = parseFloat(result[i].stats.find(stat => stat.statName === 'skdeflections').value)
        let interceptions = parseFloat(result[i].stats.find(stat => stat.statName === 'skinterceptions').value)
        let passes = parseFloat(result[i].stats.find(stat => stat.statName === 'skpasses').value)
        
        if (totalFights === 0) {
            totalFights = 1
        }

        result[i].stats.push(
            {
                hidden: false
                ,catg: "offense"
                ,fullName: "Goals per Game"
                ,statName: "skgpg"
                ,subCatg: "Scoring"
                ,subCatgSortOrder: 4
                ,type: "skater"
                ,value: (gamesPlayed == 0 ? '0.0' : (goalsScored/gamesPlayed).toFixed(2).toString())
            }
        )

        result[i].stats.push(
            {
                hidden: false
                ,catg: "offense"
                ,fullName: "Assists per Game"
                ,statName: "skapg"
                ,subCatg: "Scoring"
                ,subCatgSortOrder: 5
                ,type: "skater"
                ,value: (gamesPlayed == 0 ? '0.0' : (assistsScored/gamesPlayed).toFixed(2).toString())
            }
        )

        result[i].stats.push(
            {
                hidden: false
                ,catg: "offense"
                ,fullName: "Deke Percentage"
                ,statName: "dekesmadepct"
                ,subCatg: "Skating"
                ,subCatgSortOrder: 4
                ,type: "skater"
                ,value: (totalDekes == 0 ? '0.0' : ((dekesMade/totalDekes)*100).toFixed(2).toString())
            }
        )

        result[i].stats.push(
            {
                hidden: false
                ,catg: "offense"
                ,fullName: "Successful Dekes per Game"
                ,statName: "dekesmadepg"
                ,subCatg: "Skating"
                ,subCatgSortOrder: 5
                ,type: "skater"
                ,value: (gamesPlayed == 0 ? '0.0' : (dekesMade/gamesPlayed).toFixed(2).toString())
            }
        )

        result[i].stats.push(
            {
                hidden: false
                ,catg: "defense"
                ,fullName: "Hits per Game"
                ,statName: "skhitspg"
                ,subCatg: "Goon Play"
                ,subCatgSortOrder: 2
                ,type: "skater"
                ,value: (gamesPlayed == 0 ? '0.0' : (hitsMade/gamesPlayed).toFixed(2).toString())
            }
        )

        result[i].stats.push(
            {
                hidden: false
                ,catg: "defense"
                ,fullName: "Fight Win Percentage"
                ,statName: "skfightwinpct"
                ,subCatg: "Goon Play"
                ,subCatgSortOrder: 5
                ,type: "skater"
                ,value: (totalFights == 0 ? '0.0' : ((fightsWon/totalFights)*100).toFixed(2).toString())
            }
        )

        result[i].stats.push(
            {
                hidden: false
                ,catg: "offense"
                ,fullName: "Giveaways per Game"
                ,statName: "skgiveawayspg"
                ,subCatg: "Bloopers"
                ,subCatgSortOrder: 3
                ,type: "skater"
                ,value: (gamesPlayed == 0 ? '0.0' : (totalGiveaways/gamesPlayed).toFixed(2).toString())
            }
        )

        result[i].stats.push(
            {
                hidden: false
                ,catg: "offense"
                ,fullName: "Offsides per Game"
                ,statName: "skoffsidespg"
                ,subCatg: "Bloopers"
                ,subCatgSortOrder: 5
                ,type: "skater"
                ,value: (gamesPlayed == 0 ? '0.0' : (totalOffsides/gamesPlayed).toFixed(2).toString())
            }
        )

        result[i].stats.push(
            {
                hidden: false
                ,catg: "defense"
                ,fullName: "PIM Excluding Fights (PIMEF)"
                ,statName: "skpimef"
                ,subCatg: "Goon Play"
                ,subCatgSortOrder: 7
                ,type: "skater"
                ,value: (totalPenMins - (trueTotalFights * 5)).toString()
            }
        )

        result[i].stats.push(
            {
                hidden: false
                ,catg: "defense"
                ,fullName: "PIMEF per Game"
                ,statName: "skpimefpg"
                ,subCatg: "Goon Play"
                ,subCatgSortOrder: 8
                ,type: "skater"
                ,value: (gamesPlayed == 0 ? '0.0' : ((totalPenMins - (trueTotalFights * 5))/gamesPlayed).toFixed(2).toString())
            }
        )

        result[i].stats.push(
            {
                hidden: false
                ,catg: "offense"
                ,fullName: "Points per Game"
                ,statName: "skpointspg"
                ,subCatg: "Scoring"
                ,subCatgSortOrder: 6
                ,type: "skater"
                ,value: (gamesPlayed == 0 ? '0.0' : (totalPoints/gamesPlayed).toFixed(2).toString())
            }
        )

        result[i].stats.push(
            {
                hidden: false
                ,catg: "offense"
                ,fullName: "Shots per Game"
                ,statName: "skshotspg"
                ,subCatg: "Shooting"
                ,subCatgSortOrder: 2
                ,type: "skater"
                ,value: (gamesPlayed == 0 ? '0.0' : (totalShots/gamesPlayed).toFixed(2).toString())
            }
        )

        result[i].stats.push(
            {
                hidden: false
                ,catg: "defense"
                ,fullName: "Takeaways per Game"
                ,statName: "sktakeawayspg"
                ,subCatg: "Defending"
                ,subCatgSortOrder: 2
                ,type: "skater"
                ,value: (gamesPlayed == 0 ? '0.0' : (totalTakeaways/gamesPlayed).toFixed(2).toString())
            }
        )

        result[i].stats.push(
            {
                hidden: false
                ,catg: "offense"
                ,fullName: "Shooting %"
                ,statName: "shotpct"
                ,subCatg: "Shooting"
                ,subCatgSortOrder: 5
                ,type: "skater"
                ,value: (totalShots == 0 ? '0.0' : ((goalsScored/totalShots)*100).toFixed(2).toString())
            }
        )

        result[i].stats.push(
            {
                hidden: false
                ,catg: "offense"
                ,fullName: "Deflections per Game"
                ,statName: "deflectionspg"
                ,subCatg: "Shooting"
                ,subCatgSortOrder: 4
                ,type: "skater"
                ,value: (gamesPlayed == 0 ? '0.0' : (deflections/gamesPlayed).toFixed(2).toString())
            }
        )

        result[i].stats.push(
            {
                hidden: false
                ,catg: "defense"
                ,fullName: "Interceptions per Game"
                ,statName: "skinterceptionspg"
                ,subCatg: "Defending"
                ,subCatgSortOrder: 5
                ,type: "skater"
                ,value: (gamesPlayed == 0 ? '0.0' : (interceptions/gamesPlayed).toFixed(2).toString())
            }
        )

        result[i].stats.push(
            {
                hidden: false
                ,catg: "offense"
                ,fullName: "Pass Completions per Game"
                ,statName: "skpassespg"
                ,subCatg: "Passing"
                ,subCatgSortOrder: 4
                ,type: "skater"
                ,value: (gamesPlayed == 0 ? '0.0' : (passes/gamesPlayed).toFixed(2).toString())
            }
        )


        result[i].playerNum = i
    }

    return result
}

const obj = { populatePlayerData, updateCustomStats }

export default obj;