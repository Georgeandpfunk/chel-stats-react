const dimensions = [
    {
      statName: "skplusmin",
      fullName: "+/-",
      type: "skater",
      catg: "general",
      subCatg: "General",
      subCatgSortOrder: 5,
      hidden: true
    },
    {
      statName: "skassists",
      fullName: "Assists",
      type: "skater",
      catg: "offense",
      subCatg: "Scoring",
      subCatgSortOrder: 2,
      hidden: true
    },
    {
      statName: "skbs",
      fullName: "Blocked Shots",
      type: "skater",
      catg: "defense",
      subCatg: "Defending",
      subCatgSortOrder: 3,
      hidden: false
    },
    {
      statName: "skbreakaways",
      fullName: "Breakaways",
      type: "skater",
      catg: "offense",
      subCatg: "Skating",
      subCatgSortOrder: 14,
      hidden: false
    },
    {
      statName: "skdekes",
      fullName: "Dekes",
      type: "skater",
      catg: "offense",
      subCatg: "Skating",
      subCatgSortOrder: 2,
      hidden: false
    },
    {
      statName: "skDNF",
      fullName: "Did Not Finish",
      type: "skater",
      catg: "general",
      subCatg: "Bloopers",
      subCatgSortOrder: 1,
      hidden: false
    },
    {
      statName: "favoritePosition",
      fullName: "Favorite Position",
      type: "player",
      catg: "player",
      subCatg: "Player",
      subCatgSortOrder: 2,
      hidden: false
    },
    {
      statName: "skfightswon",
      fullName: "Fights Won",
      type: "skater",
      catg: "general",
      subCatg: "Goon Play",
      subCatgSortOrder: 4,
      hidden: false
    },
    {
      statName: "skgwg",
      fullName: "Game Winning Goals",
      type: "skater",
      catg: "offense",
      subCatg: "Scoring",
      subCatgSortOrder: 9,
      hidden: false
    },
    {
      statName: "skgp",
      fullName: "Games Played",
      type: "skater",
      catg: "general",
      subCatg: "General",
      subCatgSortOrder: 1,
      hidden: false
    },
    {
      statName: "skgiveaways",
      fullName: "Giveaways",
      type: "skater",
      catg: "general",
      subCatg: "Bloopers",
      subCatgSortOrder: 2,
      hidden: false
    },
    {
      statName: "skgoals",
      fullName: "Goals",
      type: "skater",
      catg: "offense",
      subCatg: "Scoring",
      subCatgSortOrder: 1,
      hidden: true
    },
    {
      statName: "skhattricks",
      fullName: "Hat Tricks",
      type: "skater",
      catg: "offense",
      subCatg: "Scoring",
      subCatgSortOrder: 10,
      hidden: false
    },
    {
      statName: "skhits",
      fullName: "Hits",
      type: "skater",
      catg: "defense",
      subCatg: "Goon Play",
      subCatgSortOrder: 1,
      hidden: false
    },
    {
      statName: "sklosses",
      fullName: "Losses",
      type: "skater",
      catg: "general",
      subCatg: "General",
      subCatgSortOrder: 3,
      hidden: false
    },
    {
      statName: "name",
      fullName: "Name",
      type: "player",
      catg: "player",
      subCatg: "Player",
      subCatgSortOrder: 1,
      hidden: false
    },
    {
      statName: "skoffsides",
      fullName: "Offsides",
      type: "skater",
      catg: "offense",
      subCatg: "Bloopers",
      subCatgSortOrder: 4,
      hidden: false
    },
    {
      statName: "skwinnerByDnf",
      fullName: "Opponent Disconnects",
      type: "skater",
      catg: "general",
      subCatg: "General",
      subCatgSortOrder: 6,
      hidden: false
    },
    {
      statName: "skotl",
      fullName: "Overtime Losses",
      type: "skater",
      catg: "general",
      subCatg: "General",
      subCatgSortOrder: 4,
      hidden: false
    },
    {
      statName: "skpim",
      fullName: "Penalty Mins",
      type: "skater",
      catg: "general",
      subCatg: "Goon Play",
      subCatgSortOrder: 6,
      hidden: false
    },
    {
      statName: "skpoints",
      fullName: "Points",
      type: "skater",
      catg: "offense",
      subCatg: "Scoring",
      subCatgSortOrder: 3,
      hidden: true
    },
    {
      statName: "skppg",
      fullName: "Power Play Goals",
      type: "skater",
      catg: "offense",
      subCatg: "Scoring",
      subCatgSortOrder: 7,
      hidden: false
    },
    {
      statName: "skdeflections",
      fullName: "Shot Deflections",
      type: "skater",
      catg: "offense",
      subCatg: "Shooting",
      subCatgSortOrder: 3,
      hidden: true
    },
    {
      statName: "skscrnchances",
      fullName: "Scoring Chances",
      type: "skater",
      catg: "offense",
      subCatg: "Shooting",
      subCatgSortOrder: 6,
      hidden: false
    },
    {
      statName: "skshg",
      fullName: "Shorthanded Goals",
      type: "skater",
      catg: "offense",
      subCatg: "Scoring",
      subCatgSortOrder: 8,
      hidden: false
    },
    {
      statName: "skshots",
      fullName: "Shots",
      type: "skater",
      catg: "offense",
      subCatg: "Shooting",
      subCatgSortOrder: 1,
      hidden: true
    },
    {
      statName: "skdekesmade",
      fullName: "Successful Dekes",
      type: "skater",
      catg: "offense",
      subCatg: "Skating",
      subCatgSortOrder: 3,
      hidden: false
    },
    {
      statName: "sktakeaways",
      fullName: "Takeaways",
      type: "skater",
      catg: "defense",
      subCatg: "Defending",
      subCatgSortOrder: 1,
      hidden: false
    },
    {
      statName: "skinterceptions",
      fullName: "Interceptions",
      type: "skater",
      catg: "defense",
      subCatg: "Defending",
      subCatgSortOrder: 4,
      hidden: false
    },
    {
      statName: "skpkclearzone",
      fullName: "Zone Clears During PK",
      type: "skater",
      catg: "defense",
      subCatg: "Defending",
      subCatgSortOrder: 6,
      hidden: false
    },
    {
      statName: "skfights",
      fullName: "Total Fights",
      type: "skater",
      catg: "general",
      subCatg: "Goon Play",
      subCatgSortOrder: 3,
      hidden: false
    },
    {
      statName: "skwins",
      fullName: "Wins",
      type: "skater",
      catg: "general",
      subCatg: "General",
      subCatgSortOrder: 2,
      hidden: false
    },
    {
      statName: "skpassattempts",
      fullName: "Pass Attempts",
      type: "skater",
      catg: "offense",
      subCatg: "Passing",
      subCatgSortOrder: 1,
      hidden: false
    },
    {
      statName: "skpasses",
      fullName: "Pass Completions",
      type: "skater",
      catg: "offense",
      subCatg: "Passing",
      subCatgSortOrder: 2,
      hidden: false
    },
    {
      statName: "skpasspct",
      fullName: "Completion %",
      type: "skater",
      catg: "offense",
      subCatg: "Passing",
      subCatgSortOrder: 4,
      hidden: false
    },
    {
      statName: "sksaucerpasses",
      fullName: "Saucer Passes",
      type: "skater",
      catg: "offense",
      subCatg: "Passing",
      subCatgSortOrder: 5,
      hidden: false
    }
]

const obj = { dimensions }

export default obj