import { v4 as uuidv4 } from 'uuid';

const positionTableHeaders = [
	{
		id: 1,
		position: 'center',
		columns: [
			{
				id: 1,
				dataField: 'id',
				text: 'Player Id',
				hidden: true,
				sort: false,
			},
			{
				id: 2,
				dataField: 'games',
				text: 'Games',
				hidden: true,
				sort: false,
			},
			{
				id: 3,
				dataField: 'name',
				text: 'Player Name',
				hidden: false,
				sort: false, 
				headerStyle: () => { return { width: "140px" } } 
			},
			{
				id: 4,
				dataField: 'wins',
				text: 'Wins',
				hidden: false,
				sort: true,
				headerStyle: () => { return { width: "75px" } },
				sortFunc: (a, b, order) => {
					if (order === 'asc') {
						return parseFloat(a.replace(/,/g, '')) - parseFloat(b.replace(/,/g, ''))
					}
					return parseFloat(b.replace(/,/g, '')) - parseFloat(a.replace(/,/g, ''))
				},
			},
			{
				id: 5,
				dataField: 'losses',
				text: 'Losses',
				hidden: false,
				sort: true,
				headerStyle: () => { return { width: "75px" } },
				sortFunc: (a, b, order) => {
					if (order === 'asc') {
						return parseFloat(a.replace(/,/g, '')) - parseFloat(b.replace(/,/g, ''))
					}
					return parseFloat(b.replace(/,/g, '')) - parseFloat(a.replace(/,/g, ''))
				},
			},
			{
				id: 6,
				dataField: 'winPct',
				text: 'Win %',
				hidden: false,
				sort: true,
				headerStyle: () => { return { width: "75px" } },
				sortFunc: (a, b, order) => {
					if (order === 'asc') {
						return parseFloat(a.replace(/,/g, '')) - parseFloat(b.replace(/,/g, ''))
					}
					return parseFloat(b.replace(/,/g, '')) - parseFloat(a.replace(/,/g, ''))
				},
			},
			{
				id: 7,
				dataField: 'goalsPg',
				text: 'Goals',
				hidden: false,
				sort: true,
				headerStyle: () => { return { width: "75px" } },
				sortFunc: (a, b, order) => {
					if (order === 'asc') {
						return parseFloat(a.replace(/,/g, '')) - parseFloat(b.replace(/,/g, ''))
					}
					return parseFloat(b.replace(/,/g, '')) - parseFloat(a.replace(/,/g, ''))
				},
			},
			{
				id: 8,
				dataField: 'assistsPg',
				text: 'Assists',
				hidden: false,
				sort: true,
				headerStyle: () => { return { width: "75px" } },
				sortFunc: (a, b, order) => {
					if (order === 'asc') {
						return parseFloat(a.replace(/,/g, '')) - parseFloat(b.replace(/,/g, ''))
					}
					return parseFloat(b.replace(/,/g, '')) - parseFloat(a.replace(/,/g, ''))
				},
			},
			{
				id: 9,
				dataField: 'pointsPg',
				text: 'Points',
				hidden: false,
				sort: true,
				headerStyle: () => { return { width: "75px" } },
				sortFunc: (a, b, order) => {
					if (order === 'asc') {
						return parseFloat(a.replace(/,/g, '')) - parseFloat(b.replace(/,/g, ''))
					}
					return parseFloat(b.replace(/,/g, '')) - parseFloat(a.replace(/,/g, ''))
				},
			},
			{
				id: 10,
				dataField: 'faceoffPct',
				text: 'Faceoff %',
				hidden: false,
				sort: true,
				headerStyle: () => { return { width: "75px" } },
				sortFunc: (a, b, order) => {
					if (order === 'asc') {
						return parseFloat(a.replace(/,/g, '')) - parseFloat(b.replace(/,/g, ''))
					}
					return parseFloat(b.replace(/,/g, '')) - parseFloat(a.replace(/,/g, ''))
				},
			},
		]
	},
	{
		id: 2,
		position: 'defense',
		columns: [
			{
				id: 1,
				dataField: 'id',
				text: 'Player Id',
				hidden: true,
				sort: false,
			},
			{
				id: 2,
				dataField: 'games',
				text: 'Games',
				hidden: true,
				sort: false,
			},
			{
				id: 3,
				dataField: 'name',
				text: 'Player Name',
				hidden: false,
				sort: false, 
				headerStyle: () => { return { width: "140px" } } 
			},
			{
				id: 4,
				dataField: 'wins',
				text: 'Wins',
				hidden: false,
				sort: true,
				headerStyle: () => { return { width: "75px" } },
				sortFunc: (a, b, order) => {
					if (order === 'asc') {
						return parseFloat(a.replace(/,/g, '')) - parseFloat(b.replace(/,/g, ''))
					}
					return parseFloat(b.replace(/,/g, '')) - parseFloat(a.replace(/,/g, ''))
				},
			},
			{
				id: 5,
				dataField: 'losses',
				text: 'Losses',
				hidden: false,
				sort: true,
				headerStyle: () => { return { width: "75px" } },
				sortFunc: (a, b, order) => {
					if (order === 'asc') {
						return parseFloat(a.replace(/,/g, '')) - parseFloat(b.replace(/,/g, ''))
					}
					return parseFloat(b.replace(/,/g, '')) - parseFloat(a.replace(/,/g, ''))
				},
			},
			{
				id: 6,
				dataField: 'winPct',
				text: 'Win %',
				hidden: false,
				sort: true,
				headerStyle: () => { return { width: "75px" } },
				sortFunc: (a, b, order) => {
					if (order === 'asc') {
						return parseFloat(a.replace(/,/g, '')) - parseFloat(b.replace(/,/g, ''))
					}
					return parseFloat(b.replace(/,/g, '')) - parseFloat(a.replace(/,/g, ''))
				},
			},
			{
				id: 7,
				dataField: 'goalsPg',
				text: 'Goals',
				hidden: false,
				sort: true,
				headerStyle: () => { return { width: "75px" } },
				sortFunc: (a, b, order) => {
					if (order === 'asc') {
						return parseFloat(a.replace(/,/g, '')) - parseFloat(b.replace(/,/g, ''))
					}
					return parseFloat(b.replace(/,/g, '')) - parseFloat(a.replace(/,/g, ''))
				},
			},
			{
				id: 8,
				dataField: 'assistsPg',
				text: 'Assists',
				hidden: false,
				sort: true,
				headerStyle: () => { return { width: "75px" } },
				sortFunc: (a, b, order) => {
					if (order === 'asc') {
						return parseFloat(a.replace(/,/g, '')) - parseFloat(b.replace(/,/g, ''))
					}
					return parseFloat(b.replace(/,/g, '')) - parseFloat(a.replace(/,/g, ''))
				},
			},
			{
				id: 9,
				dataField: 'pointsPg',
				text: 'Points',
				hidden: false,
				sort: true,
				headerStyle: () => { return { width: "75px" } },
				sortFunc: (a, b, order) => {
					if (order === 'asc') {
						return parseFloat(a.replace(/,/g, '')) - parseFloat(b.replace(/,/g, ''))
					}
					return parseFloat(b.replace(/,/g, '')) - parseFloat(a.replace(/,/g, ''))
				},
			},
			{
				id: 10,
				dataField: 'hitsPg',
				text: 'Hits',
				hidden: false,
				sort: true,
				headerStyle: () => { return { width: "75px" } },
				sortFunc: (a, b, order) => {
					if (order === 'asc') {
						return parseFloat(a.replace(/,/g, '')) - parseFloat(b.replace(/,/g, ''))
					}
					return parseFloat(b.replace(/,/g, '')) - parseFloat(a.replace(/,/g, ''))
				},
			},
		]
	},
	{
		id: 3,
		position: 'wing',
		columns: [
			{
				id: 1,
				dataField: 'id',
				text: 'Player Id',
				hidden: true,
				sort: false,
			},
			{
				id: 2,
				dataField: 'games',
				text: 'Games',
				hidden: true,
				sort: false,
			},
			{
				id: 3,
				dataField: 'name',
				text: 'Player Name',
				hidden: false,
				sort: false, 
				headerStyle: () => { return { width: "140px" } },
			},
			{
				id: 4,
				dataField: 'wins',
				text: 'Wins',
				hidden: false,
				sort: true,
				headerStyle: () => { return { width: "75px" } },
				sortFunc: (a, b, order) => {
					if (order === 'asc') {
						return parseFloat(a.replace(/,/g, '')) - parseFloat(b.replace(/,/g, ''))
					}
					return parseFloat(b.replace(/,/g, '')) - parseFloat(a.replace(/,/g, ''))
				},
			},
			{
				id: 5,
				dataField: 'losses',
				text: 'Losses',
				hidden: false,
				sort: true,
				headerStyle: () => { return { width: "75px" } },
				sortFunc: (a, b, order) => {
					if (order === 'asc') {
						return parseFloat(a.replace(/,/g, '')) - parseFloat(b.replace(/,/g, ''))
					}
					return parseFloat(b.replace(/,/g, '')) - parseFloat(a.replace(/,/g, ''))
				},
			},
			{
				id: 6,
				dataField: 'winPct',
				text: 'Win %',
				hidden: false,
				sort: true,
				headerStyle: () => { return { width: "75px" } },
				sortFunc: (a, b, order) => {
					if (order === 'asc') {
						return parseFloat(a.replace(/,/g, '')) - parseFloat(b.replace(/,/g, ''))
					}
					return parseFloat(b.replace(/,/g, '')) - parseFloat(a.replace(/,/g, ''))
				},
			},
			{
				id: 7,
				dataField: 'goalsPg',
				text: 'Goals',
				hidden: false,
				sort: true,
				headerStyle: () => { return { width: "75px" } },
				sortFunc: (a, b, order) => {
					if (order === 'asc') {
						return parseFloat(a.replace(/,/g, '')) - parseFloat(b.replace(/,/g, ''))
					}
					return parseFloat(b.replace(/,/g, '')) - parseFloat(a.replace(/,/g, ''))
				},
			},
			{
				id: 8,
				dataField: 'assistsPg',
				text: 'Assists',
				hidden: false,
				sort: true,
				headerStyle: () => { return { width: "75px" } },
				sortFunc: (a, b, order) => {
					if (order === 'asc') {
						return parseFloat(a.replace(/,/g, '')) - parseFloat(b.replace(/,/g, ''))
					}
					return parseFloat(b.replace(/,/g, '')) - parseFloat(a.replace(/,/g, ''))
				},
			},
			{
				id: 9,
				dataField: 'pointsPg',
				text: 'Points',
				hidden: false,
				sort: true,
				headerStyle: () => { return { width: "75px" } },
				sortFunc: (a, b, order) => {
					if (order === 'asc') {
						return parseFloat(a.replace(/,/g, '')) - parseFloat(b.replace(/,/g, ''))
					}
					return parseFloat(b.replace(/,/g, '')) - parseFloat(a.replace(/,/g, ''))
				},
			},
			{
				id: 10,
				dataField: 'hitsPg',
				text: 'Hits',
				hidden: false,
				sort: true,
				headerStyle: () => { return { width: "75px" } },
				sortFunc: (a, b, order) => {
					if (order === 'asc') {
						return parseFloat(a.replace(/,/g, '')) - parseFloat(b.replace(/,/g, ''))
					}
					return parseFloat(b.replace(/,/g, '')) - parseFloat(a.replace(/,/g, ''))
				},
			},
		]
	},
]

const translatePositions = [
	{
		position: 'leftWing',
		posSorted: '4',
		abbreviation: 'LW',
		positionGroupTitle: 'Wing',
		positionGroupValue: 'wing',
		lineGroup: 'Forward',		
	},
	{
		position: 'rightWing',
		posSorted: '3',
		abbreviation: 'RW',
		positionGroupTitle: 'Wing',
		positionGroupValue: 'wing',
		lineGroup: 'Forward',	
	},
	{
		position: 'center',
		posSorted: '5',
		abbreviation: 'C',
		positionGroupTitle: 'Center',
		positionGroupValue: 'center',
		lineGroup: 'Forward',
	},
	{
		position: 'defensemen',
		posSorted: '2',
		abbreviation: 'LD',
		positionGroupTitle: 'Defense',
		positionGroupValue: 'defense',
		lineGroup: 'Defense',
	},
	{
		position: 'defensemen',
		posSorted: '1',
		abbreviation: 'RD',
		positionGroupTitle: 'Defense',
		positionGroupValue: 'defense',
		lineGroup: 'Defense',
	},
]

const seasonSettings = {
  "1": {
    "divisionId": 1,
    "divisionName": "Division 1",
    "divisionGroupId": 3,
    "divisionGroupName": "Gold Conference",
    "pointsForPromotion": 16,
    "pointsToHoldDivision": 9,
    "pointsToTitle": 16
  },
  "2": {
    "divisionId": 2,
    "divisionName": "Division 2",
    "divisionGroupId": 3,
    "divisionGroupName": "Gold Conference",
    "pointsForPromotion": 13,
    "pointsToHoldDivision": 8,
    "pointsToTitle": 15
  },
  "3": {
    "divisionId": 3,
    "divisionName": "Division 3",
    "divisionGroupId": 2,
    "divisionGroupName": "Silver Conference",
    "pointsForPromotion": 12,
    "pointsToHoldDivision": 8,
    "pointsToTitle": 14
  },
  "4": {
    "divisionId": 4,
    "divisionName": "Division 4",
    "divisionGroupId": 2,
    "divisionGroupName": "Silver Conference",
    "pointsForPromotion": 11,
    "pointsToHoldDivision": 7,
    "pointsToTitle": 13
  },
  "5": {
    "divisionId": 5,
    "divisionName": "Division 5",
    "divisionGroupId": 2,
    "divisionGroupName": "Silver Conference",
    "pointsForPromotion": 11,
    "pointsToHoldDivision": 7,
    "pointsToTitle": 13
  },
  "6": {
    "divisionId": 6,
    "divisionName": "Division 6",
    "divisionGroupId": 2,
    "divisionGroupName": "Silver Conference",
    "pointsForPromotion": 10,
    "pointsToHoldDivision": 7,
    "pointsToTitle": 12
  },
  "7": {
    "divisionId": 7,
    "divisionName": "Division 7",
    "divisionGroupId": 1,
    "divisionGroupName": "Bronze Conference",
    "pointsForPromotion": 9,
    "pointsToHoldDivision": 6,
    "pointsToTitle": 11
  },
  "8": {
    "divisionId": 8,
    "divisionName": "Division 8",
    "divisionGroupId": 1,
    "divisionGroupName": "Bronze Conference",
    "pointsForPromotion": 8,
    "pointsToHoldDivision": 5,
    "pointsToTitle": 10
  },
  "9": {
    "divisionId": 9,
    "divisionName": "Division 9",
    "divisionGroupId": 1,
    "divisionGroupName": "Bronze Conference",
    "pointsForPromotion": 7,
    "pointsToHoldDivision": 4,
    "pointsToTitle": 9
  },
  "10": {
    "divisionId": 10,
    "divisionName": "Division 10",
    "divisionGroupId": 1,
    "divisionGroupName": "Bronze Conference",
    "pointsForPromotion": 6,
    "pointsToHoldDivision": 0,
    "pointsToTitle": 8
  }
}

const defaultCrest = 'https://media.contentapi.ea.com/content/dam/eacom/nhl/pro-clubs/custom-crests/42.png'

const clubId = '76815'

const bins = {
  matchHistoryBinId: '604287319d036f03499ec5a6',
  combinedBins: '61b917b80ddbee6f8b1dbf9e'
}

const statCols = [
	{
		id: uuidv4(),
		columnLabel: 'G',
		statName: 'goals'
	},{
		id: uuidv4(),
		columnLabel: 'A',
		statName: 'assists'
	},{
		id: uuidv4(),
		columnLabel: 'P',
		statName: 'points'
	},{
		id: uuidv4(),
		columnLabel: '+/-',
		statName: 'plusmin'
	}	
]

const winsArr = ['1','5']

const lossesArr = ['2']

const dataPointRowSpec = [
	{
		id: 1
		,propertyName: 'shots'
		,fullName: 'Shots'
		,greaterIsBetter: true
	},{
		id: 2
		,propertyName: 'hits'
		,fullName: 'Hits'
		,greaterIsBetter: true
	},{
		id: 3
		,propertyName: 'passingPct'
		,fullName: 'Passing %'
		,greaterIsBetter: true
	},{
		id: 4
		,propertyName: 'faceoffPct'
		,fullName: 'Faceoff %'
		,greaterIsBetter: true
	},{
		id: 5
		,propertyName: 'penaltyMins'
		,fullName: 'Penalty Mins'
		,greaterIsBetter: false
	}		
]

const testDashboardButtons  = [
  {
    id: 1,
    name: 'Club Overview',
    value: 'main',
    active: false
  },{
    id: 2,
    name: 'Players',
    value: 'player',
    active: false,
		options: { 
			title: 'Options',
			sortButtons: {
				title: 'Sort',
				details: [
				{
					id: 1,
					field: 'name',
					fieldName: 'Player Name',
					descending: false,
					active: false,
					alpha: true
				},{
					id: 2,
					field: 'skgoals',
					fieldName: 'Goals',
					descending: false,
					active: false,
					alpha: false
				},{
					id: 3,
					field: 'skassists',
					fieldName: 'Assists',
					descending: false,
					active: false,
					alpha: false
				},{
					id: 4,
					field: 'skpoints',
					fieldName: 'Points',
					descending: false,
					active: false,
					alpha: false
				},{
					id: 5,
					field: 'skplusmin',
					fieldName: 'Plus/Min',
					descending: false,
					active: false,
					alpha: false
				}
			],
			onClickFuctionName: 'handleSortClick'},
		selectAllButton: {
			title: 'Select All',
			onClickFuctionName: 'handleSelectClick'

		},
		toggleChartButton: {
			title: 'Show Charts',
			onClickFunctionName: 'handleDataToggleClick'
		}
	}
  },{
    id: 3,
    name: 'Recent Games',
    value: 'match',
    active: false
  }
]


const dashboardButtons = [
  {
    id: 1,
    name: 'Club Overview',
    value: 'main',
    active: false,
    route: '/club'
  },{
    id: 2,
    name: 'Players',
    value: 'player',
    active: false,
    route: '/players'
  },{
    id: 3,
    name: 'Recent Games',
    value: 'match',
    active: false,
    route: '/games'
  }
]

const optionsButtons = [
	{
		id: 1,
		title: 'Show Tables',
		value: 'tables',
		active: false,
	},
	{
		id: 2,
		title: 'Show Charts',
		value: 'charts',
		active: false,
	},
	{
		id: 3,
		title: 'Show Positional',
		value: 'positional',
		active: false,
	},
	{
		id: 4,
		title: 'Show Season Bests',
		value: 'bests',
		active: false,
	},
]

const positions = [
	{
		favoritePosition: 'center',
		positionName: 'Centerman',
	},{
		favoritePosition: 'defenseMen',
		positionName: 'Defenseman',
	},{
		favoritePosition: 'rightWing',
		positionName: 'Right Wing',
	},{
		favoritePosition: 'leftWing',
		positionName: 'Left Wing',
	},{
		favoritePosition: 'goalie',
		positionName: 'Netminder',
	}
]

const sortButtons = [
	{
		id: 1,
		field: 'name',
		fieldName: 'Player Name',
		descending: false,
		active: false,
		alpha: true
	},{
		id: 2,
		field: 'skgoals',
		fieldName: 'Goals',
		descending: false,
		active: false,
		alpha: false
	},{
		id: 3,
		field: 'skassists',
		fieldName: 'Assists',
		descending: false,
		active: false,
		alpha: false
	},{
		id: 4,
		field: 'skpoints',
		fieldName: 'Points',
		descending: false,
		active: false,
		alpha: false
	},{
		id: 5,
		field: 'skplusmin',
		fieldName: 'Plus/Min',
		descending: false,
		active: false,
		alpha: false
	}
]

const leadersSpec = [
	{
		id: 2,
		propName: 'goals',
		fullStatName: 'Goal',
		statName: 'skgoals'
	},{
		id: 3,
		propName: 'assists',
		fullStatName: 'Assist',
		statName: 'skassists'
	},{
		id: 4,
		propName: 'shots',
		fullStatName: 'Shot',
		statName: 'skshots'
	},{
		id: 5,
		propName: 'hits',
		fullStatName: 'Hit',
		statName: 'skhits'
	}
]

const tableSpec = [
	{
		id: 1,
		catgName: 'scoring',
		fullName: 'Scoring'
	},{
		id: 2,
		catgName: 'shooting',
		fullName: 'Shooting'
	},{
		id: 3,
		catgName: 'passing',
		fullName: 'Passing'
	},{
		id: 4,
		catgName: 'goon',
		fullName: 'Goon Play'
	},{
		id: 5,
		catgName: 'defending',
		fullName: 'Defending'
	}
]

const radarSpec = [
	{
		id: 1
		,statName: 'skgpg'
		,statFullName: 'Goals per Game'
	},{
		id: 2
		,statName: 'skapg'
		,statFullName: 'Assists per Game'
	},{
		id: 3
		,statName: 'dekesmadepg'
		,statFullName: 'Dekes per Game'
	},{ 
		id: 4
		,statName: 'skhitspg'
		,statFullName: 'Hits per Game'
	},{
		id: 5
		,statName: 'sktakeawayspg'
		,statFullName: 'Takeaways per Game'
	},{
		id: 6
		,statName: 'skpimefpg'
		,statFullName: 'PIMS per Game'
	}
]

const overallRecord = [
	{
		id: 6,
		prop: 'currentDivision',
		name: 'Current Division',
		url: 'https://media.contentapi.ea.com/content/dam/eacom/nhl/pro-clubs/divisioncrest/divisioncrest'
	},{
		id: 1,
		prop: 'seasons',
		name: 'Seasons Played'
	},{
		id: 2,
		prop: 'titlesWon',
		name: 'Titles Won'
	},{
		id: 3,
		prop: 'bestPoints',
		name: 'Highest Points Total'
	},{
		id: 4,
		prop: 'promotions',
		name: 'Promotions',
		icon: 'bsarrowup',
		iconColor: 'green'
	},{
		id: 5,
		prop: 'relegations',
		name: 'Relegations',
		icon: 'bsarrowdown',
		iconColor: 'red'
	}
]

const seasonPoints = [
	{
		id: 1,
		division: 1,
		stay: 8,
		title: 16	
	},{
		id: 2,
		division: 2,
		relegation: 4,
		stay: 8,
		promotion: 12,
		title: 16	
	}
]

const colorArray = [
	'rgba(72, 172, 240,',
	'rgba(1, 23, 47,',
	'rgba(168, 87, 81,',
	'rgba(216, 215, 143,',
	'rgba(144, 165, 131,',
	'rgba(72, 172, 240,',
	'rgba(1, 23, 47,',
	'rgba(168, 87, 81,',
	'rgba(216, 215, 143,',
	'rgba(144, 165, 131,',
	'rgba(72, 172, 240,',
	'rgba(1, 23, 47,',
	'rgba(168, 87, 81,',
	'rgba(216, 215, 143,',
	'rgba(144, 165, 131,',
	'rgba(72, 172, 240,',
	'rgba(1, 23, 47,',
	'rgba(168, 87, 81,',
	'rgba(216, 215, 143,',
	'rgba(144, 165, 131,',
	'rgba(72, 172, 240,',
	'rgba(1, 23, 47,',
	'rgba(168, 87, 81,',
	'rgba(216, 215, 143,',
	'rgba(144, 165, 131,'
]

const bestsDimensions = [
	{
		id: 1,
		statName: 'goals',
		displayName: 'Goals',
		cardNamePlural: ' goals',
		cardNameSingular: ' goal',
		sortOrder: 1,
	},
	{
		id: 2,
		statName: 'assists',
		displayName: 'Assists',
		cardNamePlural: ' assists',
		cardNameSingular: ' assist',
		sortOrder: 2,
	},
	{
		id: 3,
		statName: 'points',
		displayName: 'Points',
		cardNamePlural: ' points',
		cardNameSingular: ' point',
		sortOrder: 3,
	},
	{
		id: 4,
		statName: 'faceoffWinPct',
		displayName: 'Faceoff Win Rate',
		cardNamePlural: '% win rate',
		cardNameSingular: '% win rate',
		sortOrder: 4,
	},
	{
		id: 5,
		statName: 'passingPct',
		displayName: 'Passing Completion Rate',
		cardNamePlural: '% completion rate',
		cardNameSingular: '% completion rate',		
		sortOrder: 5,
	},
	{
		id: 6,
		statName: 'hits',
		displayName: 'Hits',
		cardNamePlural: ' hits',
		cardNameSingular: ' hit',
		sortOrder: 6,
	},
	{
		id: 7,
		statName: 'interceptions',
		displayName: 'Interceptions',
		cardNamePlural: ' interceptions',
		cardNameSingular: ' interception',
		sortOrder: 7,
	},
	{
		id: 8,
		statName: 'blockedShots',
		displayName: 'Blocked Shots',
		cardNamePlural: ' blocked shots',
		cardNameSingular: ' blocked shot',
		sortOrder: 8,
	},
	{
		id: 9,
		statName: 'takeaways',
		displayName: 'Takeaways',
		cardNamePlural: ' takeaways',
		cardNameSingular: ' takeaway',
		sortOrder: 9,
	},
]

const itemsPerPage = 5

const obj = { translatePositions, statCols, winsArr, lossesArr, defaultCrest, clubId, bins, dataPointRowSpec, dashboardButtons, positions, sortButtons, leadersSpec, tableSpec, overallRecord, radarSpec, colorArray, seasonSettings, testDashboardButtons, optionsButtons, positionTableHeaders, bestsDimensions, itemsPerPage }

export default obj