import { v4 as uuidv4 } from 'uuid';
import helperData from './data'

const generatePositionalArray = (positionGroupArr, activePlayersPositional) => {
	let result = []

	for ( let i = 0; i < positionGroupArr.length; i++ ) {
		let positionObj = { 
			id: i,
			positionTitle: positionGroupArr[i].charAt(0).toUpperCase() + positionGroupArr[i].slice(1),
			positionValue: positionGroupArr[i],
			players: []
		}
		
		for ( let j = 0; j < activePlayersPositional.length; j++ ) {
			if ( activePlayersPositional[j][`${positionGroupArr[i]}`].games > 0 ) {
				let playerObj = {
					name: activePlayersPositional[j].name,
					...activePlayersPositional[j][`${positionGroupArr[i]}`]
				}
				positionObj.players.push(playerObj)
			}
		}
		result.push(positionObj)
	}
	return result
}

const generateMatchData = (match, guys, clubId) => (
	{
		teamName: match.clubs[`${Object.keys(match.clubs).find(a => a === clubId)}`].details.name, 
		logoId: match.clubs[`${Object.keys(match.clubs).find(a => a === clubId)}`].details.customKit.crestAssetId,
		goals: match.clubs[`${Object.keys(match.clubs).find(a => a === clubId)}`].goals,
		shots: guys.skshots.toString(),
		hits: guys.skhits.toString(),
		passingPct: ((guys.skpasses / guys.skpassattempts) * 100).toFixed(2).toString() ,
		faceoffPct: ((guys.skfow / (guys.skfow + guys.skfol)) * 100).toFixed(2).toString(),
		penaltyMins: guys.skpim.toString()
	}
)

const generatePlayerArray = (clubId, match) => {
	const playersObj = match.players
	let result = []

	for (const [key] of Object.entries(playersObj)) {
		result.push(...Object.entries(playersObj[key]).map(x => ({...x[1], clubId: key, goodGuys: key === clubId })))
	}

	return result
}

const generateLeaders = (playerArray, leaderSpec, goodGuysClubId) => {
	let result = []

	for ( let i = 0; i < leaderSpec.length; i++ ) {
		result.push({ 
			id: i+1,
			category: leaderSpec[i].propName,
			[leaderSpec[i].propName]: playerArray.map((player,idx) => ({
				id: idx+1,
				clubId: player.clubId,
				value: parseInt(player[leaderSpec[i].statName]),
				playerName: player.playername,
				goodGuy: player.clubId === goodGuysClubId,
				positionAbbreviation: helperData.translatePositions.find(a => a.posSorted === player.posSorted).abbreviation,
			}))})
	}		

	return result
}

const generateMatchClubs = (match) => {
	let result = []

	for (const property in match.clubs) {
		result.push({ clubId: match.clubs[property].details.clubId, clubName: match.clubs[property].details.name })
	}

	return result
}

const generateStatTableHeaders = (statsArray) => {
	const headerArray = statsArray.sort((a,b) => a.subCatgSortOrder - b.subCatgSortOrder).map(stat => ({
		dataField: stat.statName,
		text: stat.fullName,
		hidden: stat.hidden,
		align: 'center',
	 	headerStyle: () => { return { width: '200px', textAlign: 'center' } },
		sort: true,
		sortFunc: (a, b, order) => {
			if (order === 'asc') {
				return parseFloat(a.replace(/,/g, '')) - parseFloat(b.replace(/,/g, ''))
			}
			return parseFloat(b.replace(/,/g, '')) - parseFloat(a.replace(/,/g, ''))
		}
	}))

	headerArray.unshift({ 
		dataField: 'name', 
		text: 'Player', 
		headerStyle: () => { return { width: "100px" } } 
	})
	headerArray.unshift({ dataField: 'id', text: 'Player Id', hidden: true })

	return headerArray
}

const generateStatTableData = (playerData, headerData) => {
	const tableData = []

	for ( let i = 0; i < playerData.length; i++ ) {
		let tableRow = { id: playerData[i].id, name: playerData[i].name }
		for ( let j = 2; j < headerData.length; j++ ) {
			tableRow[headerData[j].dataField] = numberWithCommas(playerData[i].stats.find(stat => stat.statName === headerData[j].dataField).value)
		}
		tableData.push(tableRow)
	}

	return tableData
}

const generateRadarData = (playerData, radarSpec) => {
	const dataArr = []
	const colorArray = helperData.colorArray

	for ( let i = 0; i < playerData.length; i++ ) {
		const data = {
			playerId: playerData[i].id,
			id: uuidv4(),
			labels: radarSpec.map(x => x.statFullName),
			datasets: [
				{
					label: playerData[i].name,
					data: [],
		      backgroundColor: ''.concat(colorArray[i], '0.2)'),
      		borderColor: ''.concat(colorArray[i], '1)'),
      		borderWidth: 1
				}
			]
		}

		const arr = []

		for ( let j = 0; j < radarSpec.length; j++ ) {
			for ( let k = 0; k < playerData[i].stats.length; k++ ) {
				if ( playerData[i].stats[k].statName === radarSpec[j].statName ) {
					data.datasets[0].data.push(parseFloat(playerData[i].stats[k].value))
				}
			}
		}
	
		dataArr.push(data)
	}

	return dataArr
}

const numberWithCommas = (x) => x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

const obj = { 
	generateMatchData, 
	generatePlayerArray, 
	generateLeaders, 
	generateMatchClubs, 
	generateStatTableHeaders, 
	generateStatTableData, 
	generateRadarData, 
	generatePositionalArray }

export default obj