import axios from 'axios'

const baseUrl = 'https://api.jsonbin.io/b/'
const key = process.env.REACT_APP_JSONBIN_KEY
const options = { headers: {'secret-key': key} }

const getMatchData = (binId) => {
  const req = axios.get(`${baseUrl}${binId}/latest`,options)
  return req.then(res => res.data)
}

const getCombinedData = (binId) => {
  const req = axios.get(`${baseUrl}${binId}/latest`,options)
  return req.then(res => res.data)
}

const obj = { getMatchData, getCombinedData }

export default obj