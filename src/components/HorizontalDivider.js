import React from 'react'
import { Container, Row, Col } from 'react-bootstrap'

const HorizontalDivider = ({ width, className }) => <Row className={className}><Col lg={{width}}><hr /></Col></Row>

export default HorizontalDivider;