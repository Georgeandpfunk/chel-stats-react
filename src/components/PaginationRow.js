import React from 'react'
import { Pagination, Row, Col } from 'react-bootstrap'


const PaginationRow = ({ items }) => {
	return (
    <Row className='my-auto'>
      <Col>
        <Pagination size="sm" className='d-flex'>{items}</Pagination>
      </Col>
    </Row>
	)
}

export default PaginationRow;