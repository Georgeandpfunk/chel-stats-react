import React from 'react'
import SortDropdownItem from './SortDropdownItem'
import OptionsButtonItem from './OptionsButtonItem'
import { Row, Col, Dropdown, Button, DropdownButton } from 'react-bootstrap'
import dataHelper from '../helpers/data'

const SortButtonGroup = ({ sortField, handleSortClick, selectAll, handleSelectClick, activeOptionsButton, handleOptionsButtonClick }) => {
	const buttons = dataHelper.sortButtons
	const optionsButtons = dataHelper.optionsButtons

	const activeButton = {
		field: sortField.field,
		descending: sortField.descending,
		active: true
	}

	const buttonsList = buttons.map(button => button.field !== activeButton.field ? button : { ...button, active: true, descending: activeButton.descending } )
	const optionsButtonList = optionsButtons.map(button => button.value !== activeOptionsButton ? button : { ...button, active: true })

	return (
		<>
			<Row>
				<Col>
					<DropdownButton className='d-grid gap-2' variant="outline-dark" id="dropdown-basic-button" title="Options">
						<Dropdown.Item as={Button}
							active={selectAll}
							value={selectAll}
							onClick={handleSelectClick}
						>
							Select All
						</Dropdown.Item>
						<Dropdown.Divider />
						{optionsButtonList.map(button => 
							<OptionsButtonItem
								key={button.id}
								title={button.title}
								value={button.value}
								active={button.active}
								handleOptionsButtonClick={handleOptionsButtonClick}
							/>
						)}
					</DropdownButton>	
				</Col> 
				<Col>
					<DropdownButton className='d-grid gap-2' variant="outline-dark" id="dropdown-basic-button" title="Sort Players">
			  		{buttonsList.map(button =>
			  			<SortDropdownItem
			  				key={button.id}
			  				field={button.field}
			  				fieldName={button.fieldName}
			  				descending={button.descending}
			  				active={button.active}
			  				alpha={button.alpha}
			  				handleSortClick={handleSortClick}
			  			/>
			  		)}			
					</DropdownButton>
				</Col>
			</Row>
		</>
	)
}

export default SortButtonGroup;