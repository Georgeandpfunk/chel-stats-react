import React from 'react'
import { Row, Col } from 'react-bootstrap'
import { FaArrowLeft, FaArrowRight } from 'react-icons/fa'
import { IconContext } from 'react-icons'

const MatchDetailRow = ({ goodGuysDataPoint, badGuysDataPoint, dataPointTitle, greaterIsBetter }) => {
	const rightArrow = <IconContext.Provider value={{ color: "red" }}><FaArrowRight /></IconContext.Provider>
	const leftArrow = <IconContext.Provider value={{ color: "green" }}><FaArrowLeft /></IconContext.Provider>
	const leftArrowDataPoint = <><Col xs={1} className='text-center my-auto'><h6>{leftArrow}</h6></Col><Col xs={4} className='text-center my-auto'><h6>{dataPointTitle}</h6></Col><Col xs={{span: 3, offset: 1}} className='text-center my-auto'><h6>{badGuysDataPoint}</h6></Col></>
	const rightArrowDataPoint = <><Col xs={{span: 4, offset: 1}} className='text-center my-auto'><h6>{dataPointTitle}</h6></Col><Col xs={1} className='text-center my-auto'><h6>{rightArrow}</h6></Col><Col xs={3} className='text-center my-auto'><h6>{badGuysDataPoint}</h6></Col></>
		
	const title = greaterIsBetter ? 
		parseFloat(goodGuysDataPoint) === parseFloat(badGuysDataPoint) ?
			<><Col xs= {{span: 4, offset: 1}} className='text-center my-auto'><h6>{dataPointTitle}</h6></Col><Col xs={{span: 3, offset: 1}} className='text-center my-auto'><h6>{badGuysDataPoint}</h6></Col></> :
				parseFloat(goodGuysDataPoint) > parseFloat(badGuysDataPoint) ? 
					<>{leftArrowDataPoint}</> : <>{rightArrowDataPoint}</> : 
				parseFloat(goodGuysDataPoint) < parseFloat(badGuysDataPoint) ? 
					<>{leftArrowDataPoint}</> : <>{rightArrowDataPoint}</>

	return (
		<Row>
			<Col xs={3} className='text-center my-auto'><h6>{goodGuysDataPoint}</h6></Col>
			{title}
		</Row>
	)
}

export default MatchDetailRow;