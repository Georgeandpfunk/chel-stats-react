import React from 'react'
import Col from 'react-bootstrap/Col'
import { Link, useLocation } from 'react-router-dom'

const ClickableSquare = ({ matchId, handleMatchClick, resultObj }) => {
  const squareIsSelected = useLocation().pathname === `/game/season_square/${matchId}`
  const url = squareIsSelected ? '/' : `/game/season_square/${matchId}`

  return (
    <Col className='my-auto'>
      <Link to={url} data-value={matchId} onClick={e => handleMatchClick(e.currentTarget.getAttribute('data-value').toString(), 'club')}>
        <div className={`${resultObj.className} ${squareIsSelected ? 'selected-square' : null }`}>
          <div className='content'>
            {resultObj.squareText}
          </div>
        </div>
      </Link>
    </Col>
  )
}

export default ClickableSquare;