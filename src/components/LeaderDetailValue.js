import React from 'react'
import { Row, Col } from 'react-bootstrap'

const LeaderDetailValue = ({ offset, position, name, value }) => (
	<>
		<Col xs={{span: 1, offset: offset}} className='my-auto text-center'>
			<h6><small className="text-muted">{position}</small></h6>
		</Col>
		<Col xs={3} className='my-auto'>
			<h6><small>{name} ({ value })</small></h6>
		</Col>
	</>
)

export default LeaderDetailValue