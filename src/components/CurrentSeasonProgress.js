import React from 'react'
import 'react-step-progress-bar/styles.css'
import Container from 'react-bootstrap/Container'
import { ProgressBar, Step } from 'react-step-progress-bar'
import data from '../helpers/data.js'

const CurrentSeasonProgress = ({ currentSeasonSummary }) => {
  if (currentSeasonSummary.length > 0) {
    const seasonSettings = data.seasonSettings[`${currentSeasonSummary[0].currentDivision.toString()}`]

    return (
      <div className = 'match-detail-card pt-4 pb-4'>
      <Container>
        <ProgressBar percent={25}>
          <Step>
            {({ accomplished, index }) => (
              <div className={`indexedStep ${accomplished ? "accomplished" : ""}`}>
                {index + 1}
              </div>
            )}
          </Step>
          <Step>
            {({ accomplished, index }) => (
              <div className={`indexedStep ${accomplished ? "accomplished" : ""}`}>
                {index + 1}
              </div>
            )}
          </Step>
          <Step>
            {({ accomplished, index }) => (
              <div className={`indexedStep ${accomplished ? "accomplished" : ""}`}>
                {index + 1}
              </div>
            )}
          </Step>
        </ProgressBar>
        </Container>
      </div>
    )
  } else {
    return null
  }
}

export default CurrentSeasonProgress