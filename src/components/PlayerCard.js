import React from 'react'
import StatRow from './StatRow'
import { Row, Col, Button, Image } from 'react-bootstrap'
import { Link, useLocation } from 'react-router-dom'

const PlayerCard = ({ name, stats, positionName, handlePlayerClick, playerId, activePlayer, marginClass, ...rest }) => {
	const cardIsSelected = useLocation().pathname.replace('/players/','').split('&').map(x => parseInt(x)).includes(playerId)
	const currentUrl = useLocation().pathname
	const url = 
		currentUrl === '/' ? `/players/${playerId.toString()}` : 
		currentUrl === '/players' ? `/players/${playerId.toString()}` : 
		currentUrl === `/players/${playerId.toString()}` ? '/' : 
		currentUrl.includes(`players/${playerId.toString()}&`) ? currentUrl.replace(`${playerId.toString()}&`,'') :
		( !cardIsSelected && !(currentUrl === '/') ) ? currentUrl + `&${playerId.toString()}` :
		currentUrl.replace(`&${playerId.toString()}`,'')

	return (
		<div {...rest} className={marginClass + ' d-grid gap-2'}>
			<Button as={Link} to={url} variant={ cardIsSelected ? 'primary'  : 'outline-dark'} value={playerId} onClick={e => handlePlayerClick(url)}>
				<Row>
					<Col className="my-auto" xs={3}>
						<Image className='avatar' alt='avatar' src={require(`../resources/avatars/${name.toLowerCase()}.png`).default} roundedCircle/>
					</Col>
					<Col xs={9} className='pl-4 pr-4'>
						<Row className='mt-2'>
							<Col>
								<h5>{name}</h5>
							</Col>
						</Row>
							<StatRow stats={stats.total} type={'total'} />
							{/*<StatRow stats={stats.perGame} type={'pg'} />*/}
						<Row>
							<Col>
								<h6><small>{positionName}</small></h6>
							</Col>
						</Row>
					</Col>
				</Row>
			</Button>
		</div>
	)
}

export default PlayerCard;