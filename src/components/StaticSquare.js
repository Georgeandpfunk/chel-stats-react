import React from 'react'
import Col from 'react-bootstrap/Col'

const StaticSquare = ({ resultObj }) => {
  return(
    <Col className='my-auto'>
      <div className={resultObj.className}>
        <div className='content'>
          {resultObj.squareText}
        </div>
      </div>
    </Col>
  )
}

export default StaticSquare;