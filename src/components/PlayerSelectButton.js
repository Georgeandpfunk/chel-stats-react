import React from 'react'
import Button from 'react-bootstrap/Button'

const PlayerSelectButton = ({ selectAll, handleSelectClick }) => {
	const buttonProps = selectAll ? { variant: 'primary', text: 'Deselect All' } : { variant: 'outline-dark', text: 'Select All' }

	return (
		<div className='mb-1 d-grid gap-2'>
			<Button value={selectAll} onClick={handleSelectClick} variant={buttonProps.variant}>
				{buttonProps.text}
			</Button>
		</div>
	)
}

export default PlayerSelectButton;