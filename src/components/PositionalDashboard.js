import React from 'react'
import dataHelper from '../helpers/data'
import functionHelper from '../helpers/matchDetails'
import PositionTable from './PositionTable'
import { Container, Col } from 'react-bootstrap'

const PositionalDashboard = ({ players, positionalData }) => {
	if ( players.length === 0 ) {
		return ( null )
	} else {
		const activePositionalData = positionalData.filter(player => players.includes(player.name))
		const uniquePositionGroups = [...new Set(dataHelper.translatePositions.map(item => item.positionGroupValue))]
		const positionalArray = functionHelper.generatePositionalArray(uniquePositionGroups,activePositionalData)

		return(
			<Col xl={8} lg={8} className='pb-2'>
				<Container>
					{positionalArray
						.filter(position => position.players.length > 0)
						.map(position => 
							<PositionTable
								key={position.id}
								title={position.positionTitle}
								value={position.positionValue}
								players={position.players}
							/>
					)}
				</Container>
			</Col>
		)
	}
}

export default PositionalDashboard;