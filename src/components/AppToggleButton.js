import React from 'react'
import Button from 'react-bootstrap/Button'

const AppToggleButton = ({ handleAppToggleClick, name, value, active }) => {
	const buttonVariant = active ? 'primary' : 'outline-dark'

  return (
    <div className='mb-1'>
      <Button value={value} onClick={handleAppToggleClick} variant={buttonVariant} block>
        {name}
      </Button>
    </div>
  )
}

export default AppToggleButton;