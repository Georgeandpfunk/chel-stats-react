import React from 'react'
import PlayerCardDashboard from './PlayerCardDashboard'
import PlayerDetailDashboard from './PlayerDetailDashboard'
import PlayerChartDashboard from './PlayerChartDashboard'
import PositionalDashboard from './PositionalDashboard'
import SeasonBestsDashboard from './SeasonBestsDashboard'
import SortButtonGroup from './SortButtonGroup'
import { Row, Col } from 'react-bootstrap'
import { useParams } from "react-router-dom"

const PlayerDashboard = ({ players, handleCardClick, handlePlayerClick, handleSelectClick, handleSortClick, selectAll, activePlayers, sortField, activeOptionsButton, handleOptionsButtonClick, positionalData, matches, personalBests, clubId, handlePaginationClick, bestsActivePage }) => {
  
  const playerList = useParams().playerList
  const playerListArray = playerList ? playerList.split('&').map(x => parseInt(x)) : []
  const activePlayersList = players.filter(player => playerListArray.includes(player.id))
  const activePersonalBests = personalBests.filter(player => playerListArray.includes(parseInt(player.playerId)))


  let playerDetail = null

  switch (activeOptionsButton) {
    case 'tables': {
      playerDetail = <PlayerDetailDashboard players={activePlayersList} />
      break; }
    case 'charts': {
      playerDetail = <PlayerChartDashboard players={activePlayersList} />
      break; }
    case 'positional': {
      playerDetail = <PositionalDashboard players={activePlayersList.map(x => x.name)} positionalData={positionalData} />
      break; }
    case 'bests': {
      playerDetail = <SeasonBestsDashboard personalBests={activePersonalBests} matches={matches} clubId={clubId} handlePaginationClick={handlePaginationClick} bestsActivePage={bestsActivePage}/>
      break; }
    default: {
      playerDetail = null }
  }

  return (
    <>
    <Col xl={4} lg={4}>
      <Row className='mb-2'>
        <Col>
          <SortButtonGroup 
            sortField={sortField} 
            handleSortClick={handleSortClick} 
            selectAll={selectAll}
            handleSelectClick={handleSelectClick}
            activeOptionsButton={activeOptionsButton}
            handleOptionsButtonClick={handleOptionsButtonClick}
          />
        </Col>
      </Row>
      <Row>
        <Col>
          <PlayerCardDashboard 
            players={players}
            handlePlayerClick={handlePlayerClick}
            handleCardClick={handleCardClick}
            activePlayer={activePlayersList.map(player => player.id)}
          />
        </Col>
      </Row>
    </Col>
    {playerDetail}
    </>
  )
}

export default PlayerDashboard;