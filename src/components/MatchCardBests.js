import React from 'react'
import { Button, Row, Col, Image } from 'react-bootstrap'
import { Link, useLocation } from 'react-router-dom'
import dayjs from 'dayjs'
import data from '../helpers/data.js'

const MatchCardBests = ({ matchId, opponentCrestId, opponentName, goodGuysScore, badGuysScore, timestamp, handleMatchClick, gameType, handlePaginationClick, matchIds }) => {
	const cardIsSelected = useLocation().pathname === `/game/match_card/${matchId}`
	const cardUrl = cardIsSelected ? '/' : `/game/match_card/${matchId}`
	const clubChampionship = gameType === '10' ? <Row className="club-champ"><Col><span>Club Championship</span></Col></Row> : null
	const addDefaultSrc = (e) => e.target.src = data.defaultCrest
  	const matchIndex = parseFloat(matchIds.findIndex(match => match == matchId) + 1)
  	const itemsPerPageFloat = parseFloat(data.itemsPerPage)
  	const page = Math.ceil(matchIndex/itemsPerPageFloat)

	return (
		<div className='mb-2 d-grid gap-2'>
			<Button 
				as={Link} 
				to={cardUrl} 
				variant={( parseInt(goodGuysScore) > parseInt(badGuysScore) ) ? `${cardIsSelected ? '' : 'outline-'}success` : `${cardIsSelected ? '' : 'outline-'}danger`} 
				value={matchId} 
				onClick={() => handlePaginationClick(page,'match') }
				>
				<Row>
					<Col className='my-auto' lg={3}>
						<Image className='avatar' alt='Logo N/A' onError={addDefaultSrc} src={`https://media.contentapi.ea.com/content/dam/eacom/nhl/pro-clubs/custom-crests/${opponentCrestId}.png`} roundedCircle/>
					</Col>
					<Col>
						<Row className='mt-2'>
							<Col><h5>{opponentName}</h5></Col>
						</Row>
						<Row>
						<Col>
							<h6>{( parseInt(goodGuysScore) > parseInt(badGuysScore) ) ? `(W) ${goodGuysScore} - ${badGuysScore}` : `(L) ${goodGuysScore} - ${badGuysScore}`}</h6>
							</Col>
						</Row>
						<Row className='match-date'>
							<Col>
								<span>{dayjs.unix(timestamp).format('MMMM D, YYYY')}</span>
							</Col>
						</Row>
						{clubChampionship}
					</Col>
				</Row>
			</Button>
		</div>
	)
}

export default MatchCardBests;