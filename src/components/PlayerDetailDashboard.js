import React from 'react'
import StatTable from './StatTable'
import data from '../helpers/data'
import Col from 'react-bootstrap/Col'

const PlayerDetailDashboard = ({ players }) => {
	if ( players.length === 0 ) {
		return ( null )
	} else {
		return(
			<Col xl={8} lg={8} className='pb-2'>{
				data.tableSpec.map(table =>
					<StatTable 
						key={table.id}
						tableSpec={table}
						playerData={players.map(player => (
							{
								id: player.id, 
								name: player.name, 
								position: player.position, 
								stats: player.stats.filter(stat => stat.subCatg === table.fullName)
							}))
						}
					/>
				)
				}</Col>
		)
	}
}

export default PlayerDetailDashboard;