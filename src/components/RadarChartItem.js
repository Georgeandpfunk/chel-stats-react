import React from 'react'
import { Row, Col } from 'react-bootstrap'
import { Radar } from 'react-chartjs-2'

const RadarChartItem = ({ player, values }) => {
  const playerMax = Math.max(...player.datasets[0].data.map(x => Math.ceil(x)))
  const maxValue = (values.primaryMax * 0.6) > playerMax ? values.secondaryMax : values.primaryMax
  
  const options = {
    scale: {
      ticks: { beginAtZero: true },
      suggestedMin: 0,
      suggestedMax: maxValue
    },
    plugins: { 
      legend: {
        display: false 
      }
    }
  }

  return (
    <Col xs={12} lg={6}>
    <Row>
      <Col className="d-flex justify-content-center">
        <h5>{player.datasets[0].label}</h5>
      </Col>
    </Row>
    <Row>
      <Col>
        <Radar data={player} options={options} key={player.id} />
      </Col>
    </Row>
  </Col>
  )
}

export default RadarChartItem;