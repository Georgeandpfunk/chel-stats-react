import React from 'react'
import MatchCardDashboard from './MatchCardDashboard'
import ClubDashboard from './ClubDashboard'
import MatchDetailsDashboard from './MatchDetailsDashboard'
import Col from 'react-bootstrap/Col'
import data from '../helpers/data.js'
import { useParams } from "react-router-dom"


const GameDashboard = ({ matches, clubId, handleMatchClick, matchActivePage, handlePaginationClick, seasonStats, seasonSummary }) => {
  const selectedMatchId = useParams().id
  const dashboardType = useParams().type
  const match = matches.find(match => match.matchId == selectedMatchId)
  const addDefaultSrc = (e) => e.target.src = data.defaultCrest

  const leftPane = dashboardType === 'match_card' ? (
    <Col xl={4} lg={4}>
      <MatchCardDashboard 
        matches={matches}
        clubId={clubId}
        handleMatchClick={handleMatchClick}
        selectedMatchId={selectedMatchId}
        addDefaultSrc={addDefaultSrc}
        matchActivePage={matchActivePage}
        handlePaginationClick={handlePaginationClick}
      />
    </Col>
  ) : (
    <ClubDashboard 
      seasonStats={seasonStats} 
      matches={matches}
      selectedMatchId={selectedMatchId}
      clubId={data.clubId}
      seasonSummary={seasonSummary}
      handleMatchClick={handleMatchClick}
    />
  )
  
  return (
    <>
      {leftPane}
      <Col xl={8} lg={8}>
        <MatchDetailsDashboard
          match={match}
          matchId={selectedMatchId}
          clubId={clubId}
          addDefaultSrc={addDefaultSrc}
        />
      </Col>
    </>
  )
}

export default GameDashboard;