import React from 'react'
import PlayerDashboard from './PlayerDashboard'
import MatchDashboard from './MatchDashboard'
import ClubDashboard from './ClubDashboard'
import GameDashboard from './GameDashboard'
import data from '../helpers/data.js'
import { Routes, Route } from "react-router-dom";

const AppDashboard = ({ players, activeDashboard, matches, seasonStats, seasonSummary, handleMatchClick, handlePlayerClick, handleSelectClick, handleSortClick, selectAll, sortField, matchActivePage, handlePaginationClick, activeOptionsButton, handleOptionsButtonClick, positionalData, personalBests, isMobile, bestsActivePage }) => {
  const clubDashboard = (
    <ClubDashboard 
      seasonStats={seasonStats} 
      matches={matches}
      clubId={data.clubId}
      seasonSummary={seasonSummary}
      handleMatchClick={handleMatchClick}
    />
  )

  const playerDashboard = (
    <PlayerDashboard 
      players={players}
      activeDashboard={activeDashboard}
      handlePlayerClick={handlePlayerClick}
      handleSelectClick={handleSelectClick}
      handleSortClick={handleSortClick}
      selectAll={selectAll}
      sortField={sortField}
      activeOptionsButton={activeOptionsButton}
      handleOptionsButtonClick={handleOptionsButtonClick}
      positionalData={positionalData}
      personalBests={personalBests}
      clubId={data.clubId}
      matches={matches}
      handlePaginationClick={handlePaginationClick}
      bestsActivePage={bestsActivePage}
    />
  )

  const matchDashboard = (
    <MatchDashboard 
      matches={matches}
      clubId={data.clubId}
      activeDashboard={activeDashboard}
      handleMatchClick={handleMatchClick}
      matchActivePage={matchActivePage}
      handlePaginationClick={handlePaginationClick}
      isMobile={isMobile}
    />
  )

  const gameDashboard = (
    <GameDashboard 
      matches={matches}
      clubId={data.clubId}
      activeDashboard={activeDashboard}
      handleMatchClick={handleMatchClick}
      matchActivePage={matchActivePage}
      handlePaginationClick={handlePaginationClick}
      seasonStats={seasonStats}
      seasonSummary={seasonSummary}
    />
  )

  return (
    <Routes>
      <Route path='/' element={<>{clubDashboard}{playerDashboard}{matchDashboard}</>} />
      <Route path='/games' element={matchDashboard} />
      <Route path='/players/:playerList' element={playerDashboard} />
      <Route path='/players' element={playerDashboard} />
      <Route path='/club' element={clubDashboard} />
      <Route path='/game/:type/:id' element={gameDashboard} />
    </Routes>
  )
}

export default AppDashboard