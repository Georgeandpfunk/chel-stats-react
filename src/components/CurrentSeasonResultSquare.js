import React from 'react'
import ClickableSquare from './ClickableSquare'
import StaticSquare from './StaticSquare'


const CurrentSeasonResultSquare = ({ result, matchId, handleMatchClick }) => {
	const resultObj = ( result === 'wins' ) ? { className: 'season-square win-square', squareText: 'W' } : ( result === 'losses' || result === 'otl' ) ? { className: 'season-square loss-square', squareText: 'L' } : { className: 'season-square', squareText: '-'}
	return (
		matchId ? <ClickableSquare matchId={matchId} handleMatchClick={handleMatchClick} resultObj={resultObj} /> : <StaticSquare resultObj={resultObj} />
	)
}

export default CurrentSeasonResultSquare;