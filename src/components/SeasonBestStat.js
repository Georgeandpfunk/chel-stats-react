import React from 'react'
import SeasonBestCard from './SeasonBestCard'
import { Row, Col } from 'react-bootstrap'

const SeasonBestStat = ({ dimension, personalBests, matches, clubId, handlePaginationClick }) => {
	const matchIds = matches.sort((a,b) => b.timestamp - a.timestamp).map(match => match.matchId)
	
	return (
		<>
			<Row>
				<Col>
					<h5>{dimension.displayName}</h5>
				</Col>
			</Row>
			{personalBests.filter(player => player.stat.max > 0).map(player => 
				<SeasonBestCard 
					key={parseInt(player.playerId)}
					player={player}
					dimension={dimension}
					matches={matches.filter(match => player.stat.matches.includes(match.matchId))}
					matchIds={matchIds}
					clubId={clubId}
					handlePaginationClick={handlePaginationClick}
				/>
			)}
		</>
	)
}

export default SeasonBestStat