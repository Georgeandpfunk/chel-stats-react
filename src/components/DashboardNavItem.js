import React from 'react'
import Nav from 'react-bootstrap/Nav'
import { Link } from 'react-router-dom'

const DashboardNavItem = ({ itemValue, eventKey, itemTitle, handleAppToggleClick, active, route }) => (
	<Nav.Item>
		<Nav.Link 
			as={Link}
			to={route}
			eventKey={eventKey}
			item-value={itemValue}
			item-title={itemTitle}
			onClick={handleAppToggleClick} 
			active={active}
		>
			{itemTitle}
		</Nav.Link>
	</Nav.Item>
)

export default DashboardNavItem;