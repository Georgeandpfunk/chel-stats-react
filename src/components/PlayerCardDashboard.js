import React from 'react'
import PlayerCard from './PlayerCard'
import dataHelper from '../helpers/data'
import { Flipper, Flipped } from 'react-flip-toolkit'

const PlayerCardDashboard = ({ players, handlePlayerClick, activePlayer }) => {
	const lastPlayerIndex = players.length

	return (
		<Flipper flipKey={players}>{
			players.map((player, i) => 
				<Flipped key={player.id} flipId={player.id}>
					<PlayerCard 
						key={player.id}
						name={player.name}
						stats={{ 
							total: {
								goals: player.stats.find(stat => stat.statName === 'skgoals').value,
								assists: player.stats.find(stat => stat.statName === 'skassists').value,
								points: player.stats.find(stat => stat.statName === 'skpoints').value,
								plusmin: player.stats.find(stat => stat.statName === 'skplusmin').value },
							perGame: {
								goals: (parseFloat(player.stats.find(stat => stat.statName === 'skgoals').value)/parseFloat(player.stats.find(stat => stat.statName === 'skgp').value)).toFixed(2).toString(),
								assists: (parseFloat(player.stats.find(stat => stat.statName === 'skassists').value)/parseFloat(player.stats.find(stat => stat.statName === 'skgp').value)).toFixed(2).toString(),
								points: (parseFloat(player.stats.find(stat => stat.statName === 'skpoints').value)/parseFloat(player.stats.find(stat => stat.statName === 'skgp').value)).toFixed(2).toString(),
								plusmin: (parseFloat(player.stats.find(stat => stat.statName === 'skplusmin').value)/parseFloat(player.stats.find(stat => stat.statName === 'skgp').value)).toFixed(2).toString() }}
						}
						positionName={dataHelper.positions.find(position => position.favoritePosition === player.position).positionName}
						handlePlayerClick={handlePlayerClick}
						playerId={player.id}
						activePlayer={activePlayer}
						marginClass={lastPlayerIndex === i + 1 ? 'mb-4' : 'mb-2'}
					/>
				</Flipped>
			)}
		</Flipper>
	)
}

export default PlayerCardDashboard;