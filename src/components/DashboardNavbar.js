import React from 'react'
import { Navbar, Container, Nav, Image } from 'react-bootstrap'
import DashboardNavGroup from './DashboardNavGroup'
import { Link } from 'react-router-dom'
import { useLocation } from 'react-router-dom'

const DashboardNavbar = ({ activeDashboard, handleAppToggleClick, resetNavbar }) => {
	const currentUrl = useLocation().pathname

	if ( currentUrl === '/' && activeDashboard.name !== 'main' ) {
		resetNavbar()
	}

	return (
		<Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
		  <Container>
	    	<Navbar.Brand as={Link} to='/'>
	        <Image className='d-inline-block align-top navbar-logo' alt='logo' src={require(`../resources/logo192.png`).default} />{' '}
	          Not a Big Deal
	      </Navbar.Brand>
			  <Navbar.Toggle aria-controls="responsive-navbar-nav" />
			  <Navbar.Collapse id="responsive-navbar-nav">
	        <Nav className="justify-content-start flex-grow-1 pe-3">
	          <DashboardNavGroup 
	  	      	activeDashboard={activeDashboard}
				handleAppToggleClick={handleAppToggleClick}
          	/>
      		</Nav>
	  		</Navbar.Collapse>
		  </Container>
		</Navbar>
	)
}
export default DashboardNavbar