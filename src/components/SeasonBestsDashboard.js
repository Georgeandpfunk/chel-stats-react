import React from 'react'
import data from '../helpers/data.js'
import SeasonBestStat from './SeasonBestStat'
import { Row, Col } from 'react-bootstrap'
import PaginationRow from './PaginationRow'
import paginationFunction from '../helpers/paginationFunction.js'

const SeasonBestsDashboard = ({ personalBests, matches, clubId, handlePaginationClick, bestsActivePage }) => {
	if ( personalBests.length === 0 ) {
		return ( null )
	} else {
		const generateNonZeroStatsList = (bestsDimensions, personalBests) => {
			const statList = [...new Set(bestsDimensions.map(dimension => dimension.statName))]
			let result = []

			for ( let i = 0; i < statList.length; i++ ) {
				for ( let j = 0; j < personalBests.length; j++ ) {
					if ( personalBests[j][`${statList[i]}`].max > 0 ) {
						result.push(statList[i])
					}
				}
			}

			return [... new Set(result)]
		}

		const paginationClick = (type, num) => () => {
			switch(type) {
				case 'next':
					if ( bestsActivePage < pageCount ) {
						handlePaginationClick(bestsActivePage + 1,'bests')
					}
					break;
				case 'prev':
					if ( bestsActivePage > 1 ) {
						handlePaginationClick(bestsActivePage - 1,'bests')
					}
					break;
				case 'num':
					handlePaginationClick(num,'bests')
					break;
			}
		}
		const nonZeroStatsList = generateNonZeroStatsList(data.bestsDimensions, personalBests)
		const bestsDimensions = data.bestsDimensions.filter(dim => nonZeroStatsList.includes(dim.statName)).sort((a,b) => a.sortOrder - b.sortOrder)

		const delta = 1
		const itemsPerPage = 1
		const pageCount = bestsDimensions.length

	  const displayedBestsDimensions = bestsDimensions.slice((bestsActivePage - 1) * itemsPerPage, ((bestsActivePage - 1) * itemsPerPage) + itemsPerPage)
	  const items = paginationFunction.generatePaginationItems(bestsActivePage, pageCount, delta, paginationClick)

		return (
			<>
				<Col xl={4} lg={4}>
				<Row className='my-auto'>
					<Col>
						<PaginationRow 
							activePage={bestsActivePage}
							pageCount={pageCount}
							delta={delta}
							paginationClick={paginationClick}
							items={items}
						/>
					</Col>
				</Row>
				{displayedBestsDimensions.map(dimension => 
					<SeasonBestStat
						key={dimension.id}
						dimension={dimension}
						personalBests={personalBests.map(player => ({ name: player.name, playerId: player.playerId, stat: player[`${dimension.statName}`] })).sort((a,b) => b.stat.max - a.stat.max)}
						matches={matches}
						clubId={clubId}
						handlePaginationClick={handlePaginationClick}
					/>
				)}
				</Col>
			</>
		)
	}
}

export default SeasonBestsDashboard;