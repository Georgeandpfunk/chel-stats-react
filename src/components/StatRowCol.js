import React from 'react'
import Col from 'react-bootstrap/Col'

const StatRowCol = ({ value, bold }) => ( bold ) ? <><Col xs={3}><b>{value}</b></Col></> : <><Col xs={3}>{value}</Col></>

export default StatRowCol;