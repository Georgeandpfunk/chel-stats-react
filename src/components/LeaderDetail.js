import React from 'react'
import LeaderDetailValue from './LeaderDetailValue'
import Row from 'react-bootstrap/Row'


const LeaderDetail = ({ playerName, playerMaxValue, playerPosition, goodGuyObj, rowCounterpart, totalRows, rowDiff, currentRow }) => {
	const offsetValue = currentRow > totalRows - 1 - rowDiff ? 8 : 4
	const sourceObjOffset = goodGuyObj ? 0 : offsetValue
	const counterPartOffset = goodGuyObj ? offsetValue : 0

	const sourceObj = playerMaxValue === 0 ? null : 
		<LeaderDetailValue 
			offset={sourceObjOffset}
			position={playerPosition}
			name={playerName}
			value={playerMaxValue} />

	const counterPartObj = rowCounterpart ? rowCounterpart.value === 0 ? null : 
		<LeaderDetailValue 
			offset={counterPartOffset}
			position={rowCounterpart.positionAbbreviation}
			name={rowCounterpart.playerName}
			value={rowCounterpart.value} />
	: null
	
	const detail = goodGuyObj ? (
		<>{sourceObj}{counterPartObj}</>
	) : (
		<>{counterPartObj}{sourceObj}</>
	)
	
	return (
		<><Row className='match-detail-text'>{detail}</Row></>
	)
}
export default LeaderDetail;