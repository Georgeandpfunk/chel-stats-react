import React from 'react'
import CurrentSeasonResultSquare from './CurrentSeasonResultSquare'
import { Container, Row, Col } from 'react-bootstrap'
import { v4 as uuidv4 } from 'uuid';

const CurrentSeason = ({ recentResults, matches, clubId, handleMatchClick }) => {
  
/*
  const seasonPoints = recentResults.map(result => {
    if ( result === 'wins' ) {
      return 2
    } else if ( result === 'otl' ) {
      return 1
    } else {
      return 0
    }
  }).reduce((a,b) => a + b, 0)
*/

  const clickableResults = matches.filter(match => match.clubs[`${clubId}`].cNhlOnlineGameType === '5').slice(0,recentResults.indexOf('')).reverse().map(match => ({ 
    id: match.matchId,
    result: parseInt(match.clubs[`${clubId}`].goals) > parseInt(match.clubs[`${clubId}`].goalsAgainst) ? "wins" : "losses"
  }))

  const allResults = [].concat(
    recentResults.slice(0,recentResults.indexOf('')).reverse().slice(0,recentResults.indexOf('')-clickableResults.length)
    ,clickableResults
    ,recentResults.slice(recentResults.indexOf('')))
    .map(result => ({ id: uuidv4(), matchId: result.id, result: result.result || result }))

  return (
    <div className='match-detail-card'>
      <Container>
        <Row className='pt-3'>
          <Col>
            <h4>Current Season</h4>
          </Col>
        </Row>
        <Row className='pb-2'>
          <Col>Games Remaining: {10 - recentResults.filter(result => result !== '').length}</Col>
        </Row>
        <Row className='pb-3'>
          <Col className='season-grid'>
            <Row>
              {allResults.map(result =>
                <CurrentSeasonResultSquare 
                  key={result.id} 
                  result={result.result} 
                  matchId={result.matchId} 
                  handleMatchClick={handleMatchClick}
                />
              )}
            </Row>
          </Col>
        </Row>
      </Container>
    </div>
  )
}

export default CurrentSeason