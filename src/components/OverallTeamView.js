import React from 'react'
import OverallRecordItem from './OverallRecordItem'
import dataHelper from '../helpers/data'
import { Container, Row, Col } from 'react-bootstrap'

const OverallTeamView = ({ seasonStats }) => {
  const lastRow = dataHelper.overallRecord.length

  return (
    <div className='match-detail-card'>
      <Container>
        <Row className='mt-3'>
          <Col xs={4}><h4>Overall Record</h4></Col>
          <Col className='my-auto text-right'><h4>{ (!seasonStats.record) ? '' : `(${seasonStats.record})` }</h4></Col>
        </Row>
        {dataHelper.overallRecord.map((datapoint,i) => 
          <OverallRecordItem
            key={datapoint.id}
            title={datapoint.name}
            value={seasonStats[datapoint.prop]}
            lastRow={( lastRow === i+1 )}
            icon={datapoint.icon}
            iconColor={datapoint.iconColor}
            url={datapoint.url}
          />
        )}
      </Container>
    </div>
  )
}

export default OverallTeamView