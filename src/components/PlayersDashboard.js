import React from 'react'
import PlayerCardDashboard from './PlayerCardDashboard'
import PlayerDetailDashboard from './PlayerDetailDashboard'
import PlayerChartDashboard from './PlayerChartDashboard'
import PositionalDashboard from './PositionalDashboard'
import SortButtonGroup from './SortButtonGroup'
import { Row, Col } from 'react-bootstrap'
import { useLocation } from 'react-router-dom'


const PlayersDashboard = ({ players, handleCardClick, handlePlayerClick, handleSelectClick, handleSortClick, selectAll, sortField, activeOptionsButton, handleOptionsButtonClick, positionalData }) => {
  let playerDetail = null
  const path = useLocation().pathname
  const activePlayers = players.filter(player => path.replace('/players/','').split('&').map(x => parseInt(x)).includes(player.id))

  switch (activeOptionsButton) {
    case 'tables': {
      playerDetail = <PlayerDetailDashboard players={activePlayers} />
      break; }
    case 'charts': {
      playerDetail = <PlayerChartDashboard players={activePlayers} />
      break; }
    case 'positional': {
      playerDetail = <PositionalDashboard players={activePlayers.map(x => x.name)} positionalData={positionalData} />
      break; }
    default: {
      playerDetail = null }
  }

  return (
    <>
    <Col xl={4} lg={4}>
      <Row className='mb-2'>
        <Col>
          <SortButtonGroup 
            sortField={sortField} 
            handleSortClick={handleSortClick} 
            selectAll={selectAll}
            handleSelectClick={handleSelectClick}
            activeOptionsButton={activeOptionsButton}
            handleOptionsButtonClick={handleOptionsButtonClick}
          />
        </Col>
      </Row>
      <Row>
        <Col>
          <PlayerCardDashboard 
            players={players}
            handlePlayerClick={handlePlayerClick}
            handleCardClick={handleCardClick}
            activePlayer={activePlayers.map(player => player.id)}
          />
        </Col>
      </Row>
    </Col>
    {playerDetail}
    </>
  )
}

export default PlayersDashboard;