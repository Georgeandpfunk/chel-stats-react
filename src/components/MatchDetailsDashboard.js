import React from 'react'
import MatchDetail from './MatchDetail'
import { Container, Col } from 'react-bootstrap'
import dayjs from 'dayjs'
import dataHelper from '../helpers/data'
import functionHelper from '../helpers/matchDetails'
import LeaderCard from './LeaderCard'

const MatchDetailsDashboard = ({ match, matchId, clubId, addDefaultSrc }) => {
	if (matchId === null) {
		return null
	} else {		
		const badGuysClubId = Object.keys(match.clubs).find(x => x !== clubId)
		const matchDate = dayjs.unix(match.timestamp).format('MMMM D, YYYY')
		const goodGuys = match.aggregate[`${Object.keys(match.aggregate).find(a => a === clubId)}`]
		const badGuys = match.aggregate[`${Object.keys(match.aggregate).find(a => a !== clubId)}`]
		const goodGuysData = functionHelper.generateMatchData(match, goodGuys, clubId)
		const badGuysData = functionHelper.generateMatchData(match, badGuys, badGuysClubId)
		const dataPointRowSpec = dataHelper.dataPointRowSpec
		const playerArray = functionHelper.generatePlayerArray(clubId, match)

		const leaders = functionHelper.generateLeaders(playerArray, dataHelper.leadersSpec, clubId).filter( x => x[`${x.category}`].reduce((a,b) => a + b.value, 0) !== 0 )

		return (
			<Col xs={12} className='match-detail-card'>
			<Container>
				<MatchDetail
	        goodGuysData={goodGuysData}
	        badGuysData={badGuysData}
	        matchDate={matchDate}
	        dataPointRowSpec={dataPointRowSpec}
					addDefaultSrc={addDefaultSrc}
		    />
	      {leaders.map(leader => 
	      	<LeaderCard
	      		key={leader.id} 
	      		leaderCategory={leader.category}
	      		leaderList={leader[leader.category]}
						goodGuyClubId={clubId}
	      	/>
		    )}
	      </Container>
      </Col>
		)
	}
}

export default MatchDetailsDashboard;