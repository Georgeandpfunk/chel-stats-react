import React from 'react'
import { Row, Col } from 'react-bootstrap'
import CurrentSeason from './CurrentSeason'
import OverallTeamView from './OverallTeamView'
import CurrentSeasonProgress from './CurrentSeasonProgress'

const ClubDashboard = ({ seasonStats, matches, clubId, seasonSummary, handleMatchClick }) => (
	<>
		<Col xs={12} xl={4} lg={4}>
			<Row>
				<Col className='mb-2'>
					<CurrentSeason
						recentResults={seasonStats.recentResults}
						matches={matches}
						clubId={clubId}
						handleMatchClick={handleMatchClick}
					/>
				</Col>
			</Row>
			<Row>
				<Col className='mb-4'>
					<OverallTeamView seasonStats={seasonStats} />
				</Col>	
			</Row>
			{/*
			<Row>
				<Col className='mb-1'>
					<CurrentSeasonProgress currentSeasonSummary={seasonSummary}/>
				</Col>	
			</Row>
			*/}
		</Col>
	</>
)

export default ClubDashboard;