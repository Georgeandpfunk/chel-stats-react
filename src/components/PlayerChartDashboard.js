import React from 'react'
import dataHelper from '../helpers/data'
import functionHelper from '../helpers/matchDetails'
import { Container, Row, Col } from 'react-bootstrap'
import RadarChartItem from './RadarChartItem'

const PlayerChartDashboard = ({ players }) => {
	if ( players.length === 0 ) {
		return ( null )
	} else {
		const playerChartArray = functionHelper.generateRadarData(players, dataHelper.radarSpec)
		const valuesArr = [].concat(...playerChartArray.map(x => x.datasets[0].data)).map(x => Math.ceil(x))
		const valuesObj = {
			primaryMax: Math.max(...valuesArr),
			secondaryMax:Math.max(...valuesArr.filter(x => x !== Math.max(...valuesArr)))
		}

		return(
			<Col xl={8} lg={8} className='pb-2'>
				<Container className='stat-table mb-2'>
					<Row className="d-flex justify-content-center">
							{playerChartArray.map(player => <RadarChartItem player={player} values={valuesObj} key={player.id} />)}
					</Row>
				</Container>
			</Col>
		)
	}
}

export default PlayerChartDashboard;