import React from 'react'
import { Row, Col } from 'react-bootstrap'
import BootstrapTable from 'react-bootstrap-table-next';
import data from '../helpers/data.js'
import functionHelper from '../helpers/matchDetails'
import 'bootstrap/dist/css/bootstrap.min.css';


const PositionTable = ({ title, value, players }) => {
	const headerData = data.positionTableHeaders.find(header => header.position === value).columns
		
	const defaultSort = [{
		dataField: 'games',
		order: 'desc'
	}]

  

	return (
		<>
		<Row><Col className='my-auto'><h5>{title}</h5></Col></Row>
		<BootstrapTable 
        classes='table-responsive'
        wrapperClasses='table-responsive mb-4'
				keyField='id'
        wrapperClasses='mb-4 table-responsive'
        data={players.map((player,i) => ({ ...player, id: i }))} 
        columns={headerData} 
        bordered={false}
        headerWrapperClasses='stat-table-header' 
        bodyClasses='stat-table-body'
				defaultSorted={defaultSort}
      />
		</>
	)
}

export default PositionTable;