import React from 'react'
import Row from 'react-bootstrap/Row'
import StatRowCol from './StatRowCol'
import data from '../helpers/data.js'

const StatRow = ({ stats, type }) => {
	const headerRow = type === 'total' ? <Row>{data.statCols.map(stat => <StatRowCol key={stat.id} value={stat.columnLabel} bold={true} />)}</Row> : null

	return (
		<>
			{headerRow}
			<Row>
				{data.statCols.map(stat => 
					<StatRowCol key={stat.id} value={stats[`${stat.statName}`]} bold={false} />
				)}		
			</Row>
		</>
	)
}

export default StatRow;