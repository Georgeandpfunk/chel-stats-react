import React from 'react'
import Dropdown from 'react-bootstrap/Dropdown'

const OptionsButtonItem = ({ title, value, active, handleOptionsButtonClick }) => {

	return (
		<>
			<Dropdown.Item 
				active={active}
				onClick={handleOptionsButtonClick}
				item-value={value}
			>
				{title}
			</Dropdown.Item>
		</>
	)
}

export default OptionsButtonItem