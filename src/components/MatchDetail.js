import React from 'react'
import { Row, Col, Image, Container } from 'react-bootstrap'
import MatchDetailRow from './MatchDetailRow'
import HorizontalDivider from './HorizontalDivider'

const MatchDetail = ({ goodGuysData, badGuysData, matchDate, dataPointRowSpec, addDefaultSrc }) => {
	
	return (
		<Row>
		<Col className='match-detail-text'>
		<Row><Col className='mt-3'><h4>{matchDate}</h4></Col></Row>
		<Row className='d-flex'>
			<Col xs='auto' className='align-self-center'><Image onError={addDefaultSrc} className='team-logo my-auto' alt='Logo N/A' src={`https://media.contentapi.ea.com/content/dam/eacom/nhl/pro-clubs/custom-crests/${goodGuysData.logoId}.png`} /></Col>
			<Col className='text-start align-self-center match-detail-title-row'><h5 className='align-self-center'>{goodGuysData.teamName}</h5></Col>
			<Col lg={2} xs={2} className='text-center my-auto align-self-center match-detail-title-row'><h5>{goodGuysData.goals} - {badGuysData.goals}</h5></Col>
			<Col className='text-end align-self-center match-detail-title-row'><h5>{badGuysData.teamName}</h5></Col>
			<Col xs='auto' className='align-self-center'><Image onError={addDefaultSrc} className='team-logo my-auto' alt='Logo N/A' src={`https://media.contentapi.ea.com/content/dam/eacom/nhl/pro-clubs/custom-crests/${badGuysData.logoId}.png`} /></Col>
		</Row>
		<Container><HorizontalDivider width='11' classname='justify-content-md-center'/></Container>
		{
			dataPointRowSpec.sort((a,b) => a.id - b.id).map(row => 
				<MatchDetailRow
					key={row.id}
					goodGuysDataPoint={goodGuysData[`${row.propertyName}`]}
					dataPointTitle={row.fullName}
					badGuysDataPoint={badGuysData[`${row.propertyName}`]}
					greaterIsBetter={row.greaterIsBetter}
				/>
			)
		}
		</Col>
		</Row>
	)
}

export default MatchDetail;