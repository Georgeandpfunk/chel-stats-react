import React from 'react'
import AppToggleButton from './AppToggleButton'
import { Row, Col } from 'react-bootstrap'
import dataHelper from '../helpers/data'

const DashboardButtonGroup = ({ activeDashboard, handleAppToggleClick }) => {
  const buttons = dataHelper.dashboardButtons 
  
  const activeButton = {
    value: activeDashboard,
    active: true
  }
  
  const buttonsList = buttons.map(button => button.value !== activeButton.value ? button : { ...button, active: true } )

  return (
    <>
      <Row><Col className='mt-2'></Col></Row>
      <Row className='mb-2'>
        {buttonsList.map(button => 
          <Col key={button.id} lg={2}>
            <AppToggleButton 
              key={button.id}
              handleAppToggleClick={handleAppToggleClick}
              name={button.name}
              value={button.value}
              active={button.active}
            />
          </Col>
        )}
      </Row>
    </>
  )
}

export default DashboardButtonGroup;