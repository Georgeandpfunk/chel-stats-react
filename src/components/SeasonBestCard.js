import React from 'react'
import { Row, Col, Button, Container } from 'react-bootstrap'
import MatchCardBests from './MatchCardBests'

const SeasonBestCard = ({ player, dimension, matches, matchIds, clubId, handlePaginationClick }) => {
	const gameText = player.stat.matches.length > 1 ? 'games' : 'game'
	const statName = player.stat.max > 1 ? dimension.cardNamePlural : dimension.cardNameSingular

	return (
		<>
			<Row className='mb-4'>
				<Col>
					<Row>
						<Col className='text-start'>
							<h6>{player.name}</h6>
						</Col>
						<Col className='text-end'>
							<h6><small>{player.stat.max.toString()}{statName} in {player.stat.matches.length} {gameText}</small></h6>
						</Col>
					</Row>
					{matches.map(match => 
						<MatchCardBests 
				            key={match.matchId}
				            opponentCrestId={match.clubs[`${Object.keys(match.clubs).find(a => a !== clubId)}`].details.customKit.crestAssetId}
				            opponentName={match.clubs[`${Object.keys(match.clubs).find(a => a !== clubId)}`].details.name}
				            goodGuysScore={match.clubs[`${Object.keys(match.clubs).find(a => a === clubId)}`].goals}
				            badGuysScore={match.clubs[`${Object.keys(match.clubs).find(a => a !== clubId)}`].goals}
				            timestamp={match.timestamp}
				            matchId={match.matchId}
				            gameType={match.clubs[`${Object.keys(match.clubs).find(a => a === clubId)}`].cNhlOnlineGameType}
				            handlePaginationClick={handlePaginationClick}
				            matchIds={matchIds}
						/>
					)}
				</Col>
			</Row>
		</>
	)
}

export default SeasonBestCard