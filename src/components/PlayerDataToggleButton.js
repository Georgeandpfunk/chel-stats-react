import React from 'react'
import Button from 'react-bootstrap/Button'

const PlayerDataToggleButton = ({ chartSelected, handleDataToggleClick }) => {
  const buttonText = chartSelected ? 'Show Tables' : 'Show Charts'

  return (
    <div className='mb-1 d-grid gap-2'>
      <Button value={chartSelected} onClick={handleDataToggleClick} variant="outline-dark" block>
        {buttonText}
      </Button>
    </div>
  )
}

export default PlayerDataToggleButton;