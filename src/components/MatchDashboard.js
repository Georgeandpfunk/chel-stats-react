import React from 'react'
import MatchCardDashboard from './MatchCardDashboard'
import Col from 'react-bootstrap/Col'
import data from '../helpers/data.js'


const MatchDashboard = ({ matches, clubId, handleMatchClick, matchActivePage, handlePaginationClick, isMobile }) => {
  const addDefaultSrc = (e) => e.target.src = data.defaultCrest

  return (
    <>
      <Col xl={4} lg={4}>
        <MatchCardDashboard 
          matches={matches}
          clubId={clubId}
          handleMatchClick={handleMatchClick}
          addDefaultSrc={addDefaultSrc}
          matchActivePage={matchActivePage}
          handlePaginationClick={handlePaginationClick}
          isMobile={isMobile}
        />
      </Col>
    </>
  )
}

export default MatchDashboard;