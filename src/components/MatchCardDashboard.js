import React from 'react'
import MatchCard from './MatchCard'
import PaginationRow from './PaginationRow'
import paginationFunction from '../helpers/paginationFunction.js'

const MatchCardDashboard = ({ matches, clubId, handleMatchClick, addDefaultSrc, matchActivePage, handlePaginationClick, isMobile }) => {
   
  const itemsPerPage = 5
  const pageCount = Math.ceil(matches.length/itemsPerPage)
  const delta = isMobile ? 1 : 2
  
  const paginationClick = (type, num) => () => {
    switch(type) {
      case 'next':
        if ( matchActivePage < pageCount ) {
          handlePaginationClick(matchActivePage + 1,'match')
        }
        break;
      case 'prev':
        if ( matchActivePage > 1 ) {
          handlePaginationClick(matchActivePage - 1,'match')
        }
        break;
      case 'num':
        handlePaginationClick(num,'match')
        break;
    }
  }

  const displayedMatches = matches.sort((a,b) => b.timestamp - a.timestamp).slice((matchActivePage - 1) * itemsPerPage, ((matchActivePage - 1) * itemsPerPage) + itemsPerPage)
  const paginationItems = paginationFunction.generatePaginationItems(matchActivePage, pageCount, delta, paginationClick)

  return (
    <>
    <PaginationRow 
      activePage={matchActivePage}
      paginationClick={paginationClick}
      pageCount={pageCount}
      delta={delta}
      items={paginationItems}
    />
    {
      displayedMatches.map(match => (
          <MatchCard 
            key={match.matchId}
            opponentCrestId={match.clubs[`${Object.keys(match.clubs).find(a => a !== clubId)}`].details.customKit.crestAssetId}
            opponentName={match.clubs[`${Object.keys(match.clubs).find(a => a !== clubId)}`].details.name}
            goodGuysScore={match.clubs[`${Object.keys(match.clubs).find(a => a === clubId)}`].goals}
            badGuysScore={match.clubs[`${Object.keys(match.clubs).find(a => a !== clubId)}`].goals}
            timestamp={match.timestamp}
            handleMatchClick={handleMatchClick}
            matchId={match.matchId}
            gameType={match.clubs[`${Object.keys(match.clubs).find(a => a === clubId)}`].cNhlOnlineGameType}
            addDefaultSrc={addDefaultSrc}
          />
        ))
      }
    </>
	)
}

export default MatchCardDashboard;