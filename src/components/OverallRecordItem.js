import React from 'react'
import { Row, Col, Image } from 'react-bootstrap'
import HorizontalDivider from './HorizontalDivider'
import { BsArrowUp, BsArrowDown } from "react-icons/bs";
import { IconContext } from "react-icons";

const OverallRecordItem = ({ title, value, lastRow, icon, iconColor, url }) => {
	const rowClass = lastRow ? 'pb-3' : ''
	
	const icons = {
		bsarrowup: BsArrowUp,
		bsarrowdown: BsArrowDown
	}

	const IconComponent = icons[icon]

	return (
		<>
			<HorizontalDivider width={10} />
			<Row className={rowClass}>
				<Col>
					<Row>
						<Col className='my-auto' lg={9} xs={7}>
							{title}
							{icon ? ( <IconContext.Provider value={{color: iconColor}}><IconComponent /></IconContext.Provider> ) : null}
						</Col>
						<Col className='text-center my-auto' lg={3} xs={5}>
							{ url ? <Image className='d-inline-block align-center division-logo' alt=' ' src={`${url}${value}.png`} /> : null }
							{ url ? null : value}
						</Col>
					</Row>
				</Col>
			</Row>
		</>
	)
}

export default OverallRecordItem;