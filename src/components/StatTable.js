import React from 'react'
import BootstrapTable from 'react-bootstrap-table-next';
import functionHelper from '../helpers/matchDetails'
import { Row, Col } from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';

const StatTable = ({ tableSpec, playerData }) => {
  const headerData = functionHelper.generateStatTableHeaders(playerData[0].stats)
  const tableData = functionHelper.generateStatTableData(playerData, headerData)

	return (
    <>
      <Row><Col className='mb-2'><h5><b>{tableSpec.fullName}</b></h5></Col></Row>
      <BootstrapTable 
        classes='table-responsive'
        wrapperClasses='table-responsive mb-4'
        keyField='id'
        data={tableData} 
        columns={headerData} 
        bordered={false} 
        headerWrapperClasses='stat-table-header' 
        bodyClasses='stat-table-body'
      />
    </>
	)
}

export default StatTable;