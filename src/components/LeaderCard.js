import React from 'react'
import HorizontalDivider from './HorizontalDivider'
import { Row, Col } from 'react-bootstrap'
import dataHelper from '../helpers/data'
import LeaderDetail from './LeaderDetail'

const LeaderCard = ({ leaderCategory, leaderList, goodGuyClubId }) => {
	const leaderSpec = dataHelper.leadersSpec.find(spec => spec.propName === leaderCategory)

	const maxValue = { 
		goodGuyMax: Math.max(...leaderList.filter(player => player.clubId === goodGuyClubId).map(o => o.value), 0),
		badGuyMax: Math.max(...leaderList.filter(player => player.clubId !== goodGuyClubId).map(o => o.value), 0)
	}
	
	const leaders = { 
		goodGuyLeaders: leaderList.filter(player => player.value === maxValue.goodGuyMax && player.clubId === goodGuyClubId).map(x => ({ ...x, maxValue: maxValue.goodGuyMax })),
		badGuyLeaders: leaderList.filter(player => player.value === maxValue.badGuyMax && player.clubId !== goodGuyClubId).map(x => ({ ...x, maxValue: maxValue.badGuyMax }))
	}

	const goodGuyLeaders = leaders.goodGuyLeaders.filter(player => player.value > 0)
	const badGuyLeaders = leaders.badGuyLeaders.filter(player => player.value > 0)

	const maxLenProperty = goodGuyLeaders.length >= badGuyLeaders.length ? 'goodGuyLeaders' : 'badGuyLeaders'
	const minLenProperty = goodGuyLeaders.length >= badGuyLeaders.length ? 'badGuyLeaders' : 'goodGuyLeaders'
	const maxLenLeadersListPlayers = leaders[`${maxLenProperty}`].filter(player => player.value > 0)
	const minLenLeadersListPlayers = leaders[`${minLenProperty}`].filter(player => player.value > 0)

	const leaderArray = leaders[`${maxLenProperty}`].map((x,i) => ( { ...x, counterPart: leaders[`${minLenProperty}`][i] } ))

	const totalRows = maxLenLeadersListPlayers.length
	const rowDiff = totalRows - minLenLeadersListPlayers.length

	return (
		<>
		<HorizontalDivider width='11'/>
			<Row className='match-detail-text'><Col><h5>{leaderSpec.fullStatName} {(leaderArray.length > 1) ? 'Leaders' : 'Leader' }</h5></Col></Row>
			{leaderArray.map((leader,i) => 
				<LeaderDetail
					key={leader.id}
					playerName={leader.playerName}
					playerMaxValue={leader.maxValue}
					playerPosition={leader.positionAbbreviation}
					goodGuyObj={leader.goodGuy}
					rowCounterpart={leader.counterPart}
					totalRows={totalRows}
					rowDiff={rowDiff}
					currentRow={i}
				/>
			)}
		</>
	)
}

export default LeaderCard;