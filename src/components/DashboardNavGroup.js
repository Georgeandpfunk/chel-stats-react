import React from 'react'
import DashboardNavItem from './DashboardNavItem'
import data from '../helpers/data.js'

const DashboardNavGroup = ({ activeDashboard, handleAppToggleClick }) => {
  const activeDashboardName = activeDashboard.name === 'club' ? 'main' : activeDashboard.name

  const activeButton = {
    value: activeDashboardName,
    active: true
  }

  const buttonsList = data.dashboardButtons.map(dashboardListItem => dashboardListItem.value !== activeButton.value ? dashboardListItem : { ...dashboardListItem, active: true } )

  return (
    <>
      {buttonsList.map(dashboardListItem => 
        <DashboardNavItem 
          key={dashboardListItem.id}
          eventKey={dashboardListItem.id}
          itemValue={dashboardListItem.value}
          itemTitle={dashboardListItem.name}
          handleAppToggleClick={handleAppToggleClick}
          active={dashboardListItem.active}
          route={dashboardListItem.route}
        />
      )}
    </>
  )
}

export default DashboardNavGroup;