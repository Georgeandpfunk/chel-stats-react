import React, { useState, useEffect } from 'react'
import { useNavigate, useLocation } from 'react-router-dom'
import chelService from './services/playerData'
import AppDashboard from './components/AppDashboard'
import DashboardNavbar from './components/DashboardNavbar'
import { Container, Row, Col } from 'react-bootstrap'
import data from './helpers/data.js'
import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css';

const App = () => {
  /* EA Data */
  const [ players, setPlayerData ] = useState([])
  const [ matches, setMatchData ] = useState([])
  const [ seasonStats, setSeasonStats ] = useState({recentResults:[]})
  const [ seasonSummary, setSeasonSummary ] = useState([])
  const [ positionalData, setPositionalData ] = useState([])
  const [ personalBests, setPersonalBests ] = useState([])

  /* App dashboard state */
  const [ activeDashboard, setActiveDashboard ] = useState({name: 'main', title: 'Club Overview'})
  const [ activeOptionsButton, setActiveOptionsButton ] = useState('tables')

  /* Player dashboard state */
  const [ selectAll, setSelectAll ] = useState(false)
  const [ sortField, setSortField ] = useState({})

  /* Pagination state */
  const [ matchActivePage, setMatchActivePage ] = useState(1)
  const [ bestsActivePage, setBestsActivePage ] = useState(1)

  /* Detect mobile viewport */
  const [ width, setWidth ] = useState(window.innerWidth)

  // React router DOM
  const navigate = useNavigate()

  // Sorts the player state after the sortField state has been updated
  useEffect(() => {
    sortPlayerState(sortField)
  },[sortField])

  // retrieve match history from database
  useEffect(() => {
    chelService
      .getMatchData(data.bins.matchHistoryBinId)
      .then(initialData => {
        setMatchData(initialData)
      })
  }, [])

  //retrieve combined bins
    useEffect(() => {
      chelService
      .getCombinedData(data.bins.combinedBins)
      .then(initialData => {
        setPositionalData(initialData.positionalData)
        setSeasonSummary(initialData.seasonSummaryData)
        setSeasonStats(initialData.seasonData[0])
        setPlayerData(initialData.playerData.map(member => ({ ...member, id: parseInt(member.id) })))
        setPersonalBests(initialData.personalBests)
      })
    }, [])

  //determine device/window size
  useEffect(() => {
    window.addEventListener('resize', handleWindowSizeChange)
    return () => {
        window.removeEventListener('resize', handleWindowSizeChange)
    }
  }, [])

  const currentPath = useLocation().pathname

  const handleWindowSizeChange = () => {
    setWidth(window.innerWidth);
  }

  const isMobile = width <= 768;

  // Handles clicking of the navbar values (club overview/players/recent games)
  const handleAppToggleClick = (e) => {
    setActiveDashboard({ name: e.target.getAttribute('item-value').toString(), title: e.target.getAttribute('item-title') })
    if ( e.target.getAttribute('item-value').toString() === 'main' ) {
      setSelectAll(false)
    } else {
      return
    }
  }

  // Handles clicking of the player cards
  const handlePlayerClick = (url) => {
    if ( url === '/' ) {
      handleCardClick('player', false)
      setSelectAll(false)
    } else if ( url === '/players/'.concat(`${players.map(player => player.id.toString()).join('&')}`) ){
      handleCardClick('player', true)
      setSelectAll(true)
    } else {
      handleCardClick('player', true)
    }
  }
  
  // Handles clicking of the "select all/deselect all" button above the player cards
  const handleSelectClick = (e) => {
    setSelectAll(!(e.target.value === 'true'))
    if (!(e.target.value === 'true')) {
      navigate('/players/'.concat(`${players.map(player => player.id.toString()).join('&')}`))
      handleCardClick('player', true)
    } else { 
      handleCardClick('player', false)
    }
  }  

  /* Sub-routine for the click on player or match card. Tells the app to display a
     specific dashboard, or to display the entire view if all cards are de-selected. */
  const handleCardClick = (type, active) => {
    if ( type === 'match') {
      if ( activeDashboard.name != 'match' || active ) {
        setActiveDashboard({ name:  'match', title: 'Recent Games'})
      } else {
        setActiveDashboard({ name: 'main', title: 'Club Overview' })
        navigate('/')
      }
    } else if ( type === 'player' ) {
      if ( activeDashboard.name != 'player' || active ) {
        setActiveDashboard({ name:  'player', title: 'Players'})
      } else {
        setActiveDashboard({ name: 'main', title: 'Club Overview' })
        navigate('/')
      }
    } else if ( type === 'club' ) {
      if ( activeDashboard.name != 'club' || active ) {
        setActiveDashboard({ name:  'club', title: 'Club Overview'})
      } else {
        setActiveDashboard({ name: 'main', title: 'Club Overview' })
        navigate('/')
      }
    } else {
      return
    }
  }

  // Sorts the playercards when the sort button is clicked
  const handleSortClick = (e) => setSortField({ field: e.target.getAttribute('item-value'), descending: !(e.target.getAttribute('descending') === 'true'), alpha: (e.target.getAttribute('alpha') === 'true') })

  // Handles the click on a match card to show the match details
  const handleMatchClick = (cardIsSelected, source) => {
    const dashboard = source === 'club' ? 'club' : 'match'
    setSelectAll(false)
    if ( cardIsSelected ) {
      handleCardClick(dashboard, false)
    } else {
      handleCardClick(dashboard, true)
    }
  }

  const handlePaginationClick = (n,type) => {
    switch (type) {
      case 'match':
        setMatchActivePage(n)
        break;
      case 'bests':
        setBestsActivePage(n)
        break;
      default:
        break;
    }
  }

  // This function is called by an effect after the sortField state is changed
  const sortPlayerState = (sortField) => {
    if ( players && sortField.field ) {
      if (sortField.alpha) {
       if ( sortField.descending ) {
          setPlayerData([...players].sort((b, a) => a.stats.find(stat => stat.statName === `${sortField.field}`).value.localeCompare(b.stats.find(stat => stat.statName === `${sortField.field}`).value)))
        } else {
          setPlayerData([...players].sort((a, b) => a.stats.find(stat => stat.statName === `${sortField.field}`).value.localeCompare(b.stats.find(stat => stat.statName === `${sortField.field}`).value)))
        }     
      } else {
        if ( sortField.descending ) {
          setPlayerData([...players].sort((b, a) => parseInt(a.stats.find(stat => stat.statName === `${sortField.field}`).value) - parseInt(b.stats.find(stat => stat.statName === `${sortField.field}`).value)))
        } else {
          setPlayerData([...players].sort((a, b) => parseInt(a.stats.find(stat => stat.statName === `${sortField.field}`).value) - parseInt(b.stats.find(stat => stat.statName === `${sortField.field}`).value)))
        }
      }
    }
  }

  // if "show charts" or "show radar" buttons are cliked and no players are currently selected, select all players
  const handleOptionsButtonClick = (e) => {
    setActiveOptionsButton(e.target.getAttribute('item-value'))
    if ( currentPath === '/' ) {
      setSelectAll(!(e.target.value === 'true'))
      const url = '/players/'.concat(`${players.map(player => player.id.toString()).join('&')}`)
      navigate(url)
      handleCardClick('player', true)
    }
  }

  const resetNavbar = () => setActiveDashboard({name: 'main', title: 'Club Overview'})

  return (
    <>
      <DashboardNavbar 
        activeDashboard={activeDashboard}
        handleAppToggleClick={handleAppToggleClick}
        resetNavbar={resetNavbar}
      />
      <Container fluid="lg">
        <Container fluid>
          <Row><Col className='mt-2'></Col></Row>
            <Container fluid className='mb-2'>
            <Row>
              <AppDashboard 
                handleAppToggleClick={handleAppToggleClick}
                players={players}
                activeDashboard={activeDashboard}
                matches={matches}
                seasonStats={seasonStats}
                seasonSummary={seasonSummary}
                handleMatchClick={handleMatchClick}
                handlePlayerClick={handlePlayerClick}
                handleSelectClick={handleSelectClick}
                handleSortClick={handleSortClick}
                selectAll={selectAll}
                sortField={sortField}
                matchActivePage={matchActivePage}
                bestsActivePage={bestsActivePage}
                handlePaginationClick={handlePaginationClick}
                activeOptionsButton={activeOptionsButton}
                handleOptionsButtonClick={handleOptionsButtonClick}
                positionalData={positionalData}
                personalBests={personalBests}
                isMobile={isMobile}
              />
            </Row>
          </Container>
        </Container>
      </Container>
    </>
  )
}

export default App;