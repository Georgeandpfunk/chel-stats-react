const matchData = require('./match-history.json')
const newMatchData = require('./temp.json')
const fs = require('fs')
const numberOfMatches = 200
const joinMatchData = (newData, data) => {
  data.unshift(...newData)
  return data
}

const uniqueifyMatchData = (data) => data.filter((v,i,a)=>a.findIndex(t=>(t.matchId === v.matchId))===i)

const exportMatches = (data, n) => data.filter((v,i,a)=>a.findIndex(t=>(t.matchId === v.matchId))===i).slice(0,n - 1)

const reduceMatchData = (data) => {
  let newMatchData = []

  data.forEach(match => {
    let newMatch = {
      matchId: match.matchId,
      timestamp: match.timestamp
    }
    
    let newClubObj = {}

    for ( const [key] of Object.entries(match.clubs) ) {
      newClubObj[`${key}`] = {
        cNhlOnlineGameType: match.clubs[`${key}`].cNhlOnlineGameType,
        goals: match.clubs[`${key}`].goals,
        goalsAgainst: match.clubs[`${key}`].goalsAgainst,
        result: match.clubs[`${key}`].result,
        details: { name: match.clubs[`${key}`].details.name, customKit: { crestAssetId: match.clubs[`${key}`].details.customKit.crestAssetId } }
      }
    }

    newMatch.clubs = newClubObj

    let newPlayerClubObj = {}

    for ( const [key] of Object.entries(match.players) ) {
      newPlayerClubObj[`${key}`] = { }
      for ( const [playerKey] of Object.entries(match.players[`${key}`]) ) {
        newPlayerClubObj[`${key}`][`${playerKey}`] = {
          skshots: match.players[`${key}`][`${playerKey}`].skshots,
          skhits: match.players[`${key}`][`${playerKey}`].skhits,
          skpasses: match.players[`${key}`][`${playerKey}`].skpasses,
          skpassattempts: match.players[`${key}`][`${playerKey}`].skpassattempts,
          skfow: match.players[`${key}`][`${playerKey}`].skfow,
          skfol: match.players[`${key}`][`${playerKey}`].skfol,
          skpim: match.players[`${key}`][`${playerKey}`].skpim,
          skgoals: match.players[`${key}`][`${playerKey}`].skgoals,
          skassists: match.players[`${key}`][`${playerKey}`].skassists,
          skinterceptions: match.players[`${key}`][`${playerKey}`].skinterceptions,
          skbs: match.players[`${key}`][`${playerKey}`].skbs,
          sktakeaways: match.players[`${key}`][`${playerKey}`].sktakeaways,
          skpasspct: match.players[`${key}`][`${playerKey}`].skpasspct,
          playername: match.players[`${key}`][`${playerKey}`].playername,
          position: match.players[`${key}`][`${playerKey}`].position,
          posSorted: match.players[`${key}`][`${playerKey}`].posSorted,
        }
      }
    }

    newMatch.players = newPlayerClubObj
    
    let newAggObj = {}

    for ( const [key] of Object.entries(match.aggregate) ) {
      newAggObj[`${key}`] = {
        skshots: match.aggregate[`${key}`].skshots,
        skhits: match.aggregate[`${key}`].skhits,
        skpasses: match.aggregate[`${key}`].skpasses,
        skpassattempts: match.aggregate[`${key}`].skpassattempts,
        skfow: match.aggregate[`${key}`].skfow,
        skfol: match.aggregate[`${key}`].skfol,
        skpim: match.aggregate[`${key}`].skpim,
      }
    }
    newMatch.aggregate = newAggObj

    newMatchData.push(newMatch)
  })

  return newMatchData
}

fs.writeFile('match-history.json', JSON.stringify(uniqueifyMatchData(joinMatchData(newMatchData, matchData)),null,2), 'utf8', (err) => {
  if (err) {
    console.log(err)}
  else {
    console.log("File written successfully\n")
  }
})

fs.writeFile('match-update.json', JSON.stringify(reduceMatchData(exportMatches(joinMatchData(newMatchData, matchData), numberOfMatches))), 'utf8', (err) => {
  if (err) {
    console.log(err)}
  else {
    console.log("File written successfully\n")
  }
})