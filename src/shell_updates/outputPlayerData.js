const playerData = require('./temp-player-data.json')
const personalBests = require('./personal-bests.json')
const fs = require('fs')

const dimensions = [
    {
      statName: "skplusmin",
      fullName: "+/-",
      type: "skater",
      catg: "general",
      subCatg: "General",
      subCatgSortOrder: 5,
      hidden: true
    },
    {
      statName: "skassists",
      fullName: "Assists",
      type: "skater",
      catg: "offense",
      subCatg: "Scoring",
      subCatgSortOrder: 2,
      hidden: true
    },
    {
      statName: "skbs",
      fullName: "Blocked Shots",
      type: "skater",
      catg: "defense",
      subCatg: "Defending",
      subCatgSortOrder: 3,
      hidden: false
    },
    {
      statName: "skbreakaways",
      fullName: "Breakaways",
      type: "skater",
      catg: "offense",
      subCatg: "Skating",
      subCatgSortOrder: 14,
      hidden: false
    },
    {
      statName: "skdekes",
      fullName: "Dekes",
      type: "skater",
      catg: "offense",
      subCatg: "Skating",
      subCatgSortOrder: 2,
      hidden: false
    },
    {
      statName: "skDNF",
      fullName: "Did Not Finish",
      type: "skater",
      catg: "general",
      subCatg: "Bloopers",
      subCatgSortOrder: 1,
      hidden: false
    },
    {
      statName: "favoritePosition",
      fullName: "Favorite Position",
      type: "player",
      catg: "player",
      subCatg: "Player",
      subCatgSortOrder: 2,
      hidden: false
    },
    {
      statName: "skfightswon",
      fullName: "Fights Won",
      type: "skater",
      catg: "general",
      subCatg: "Goon Play",
      subCatgSortOrder: 4,
      hidden: false
    },
    {
      statName: "skgwg",
      fullName: "Game Winning Goals",
      type: "skater",
      catg: "offense",
      subCatg: "Scoring",
      subCatgSortOrder: 9,
      hidden: false
    },
    {
      statName: "skgp",
      fullName: "Games Played",
      type: "skater",
      catg: "general",
      subCatg: "General",
      subCatgSortOrder: 1,
      hidden: false
    },
    {
      statName: "skgiveaways",
      fullName: "Giveaways",
      type: "skater",
      catg: "general",
      subCatg: "Bloopers",
      subCatgSortOrder: 2,
      hidden: false
    },
    {
      statName: "skgoals",
      fullName: "Goals",
      type: "skater",
      catg: "offense",
      subCatg: "Scoring",
      subCatgSortOrder: 1,
      hidden: true
    },
    {
      statName: "skhattricks",
      fullName: "Hat Tricks",
      type: "skater",
      catg: "offense",
      subCatg: "Scoring",
      subCatgSortOrder: 10,
      hidden: false
    },
    {
      statName: "skhits",
      fullName: "Hits",
      type: "skater",
      catg: "defense",
      subCatg: "Goon Play",
      subCatgSortOrder: 1,
      hidden: false
    },
    {
      statName: "sklosses",
      fullName: "Losses",
      type: "skater",
      catg: "general",
      subCatg: "General",
      subCatgSortOrder: 3,
      hidden: false
    },
    {
      statName: "name",
      fullName: "Name",
      type: "player",
      catg: "player",
      subCatg: "Player",
      subCatgSortOrder: 1,
      hidden: false
    },
    {
      statName: "skoffsides",
      fullName: "Offsides",
      type: "skater",
      catg: "offense",
      subCatg: "Bloopers",
      subCatgSortOrder: 4,
      hidden: false
    },
    {
      statName: "skwinnerByDnf",
      fullName: "Opponent Disconnects",
      type: "skater",
      catg: "general",
      subCatg: "General",
      subCatgSortOrder: 6,
      hidden: false
    },
    {
      statName: "skotl",
      fullName: "Overtime Losses",
      type: "skater",
      catg: "general",
      subCatg: "General",
      subCatgSortOrder: 4,
      hidden: false
    },
    {
      statName: "skpim",
      fullName: "Penalty Mins",
      type: "skater",
      catg: "general",
      subCatg: "Goon Play",
      subCatgSortOrder: 6,
      hidden: false
    },
    {
      statName: "skpoints",
      fullName: "Points",
      type: "skater",
      catg: "offense",
      subCatg: "Scoring",
      subCatgSortOrder: 3,
      hidden: true
    },
    {
      statName: "skppg",
      fullName: "Power Play Goals",
      type: "skater",
      catg: "offense",
      subCatg: "Scoring",
      subCatgSortOrder: 7,
      hidden: false
    },
    {
      statName: "skdeflections",
      fullName: "Shot Deflections",
      type: "skater",
      catg: "offense",
      subCatg: "Shooting",
      subCatgSortOrder: 3,
      hidden: true
    },
    {
      statName: "skscrnchances",
      fullName: "Scoring Chances",
      type: "skater",
      catg: "offense",
      subCatg: "Shooting",
      subCatgSortOrder: 6,
      hidden: false
    },
    {
      statName: "skshg",
      fullName: "Shorthanded Goals",
      type: "skater",
      catg: "offense",
      subCatg: "Scoring",
      subCatgSortOrder: 8,
      hidden: false
    },
    {
      statName: "skshots",
      fullName: "Shots",
      type: "skater",
      catg: "offense",
      subCatg: "Shooting",
      subCatgSortOrder: 1,
      hidden: true
    },
    {
      statName: "skdekesmade",
      fullName: "Successful Dekes",
      type: "skater",
      catg: "offense",
      subCatg: "Skating",
      subCatgSortOrder: 3,
      hidden: false
    },
    {
      statName: "sktakeaways",
      fullName: "Takeaways",
      type: "skater",
      catg: "defense",
      subCatg: "Defending",
      subCatgSortOrder: 1,
      hidden: false
    },
    {
      statName: "skinterceptions",
      fullName: "Interceptions",
      type: "skater",
      catg: "defense",
      subCatg: "Defending",
      subCatgSortOrder: 4,
      hidden: false
    },
    {
      statName: "skpkclearzone",
      fullName: "Zone Clears During PK",
      type: "skater",
      catg: "defense",
      subCatg: "Defending",
      subCatgSortOrder: 6,
      hidden: false
    },
    {
      statName: "skfights",
      fullName: "Total Fights",
      type: "skater",
      catg: "general",
      subCatg: "Goon Play",
      subCatgSortOrder: 3,
      hidden: false
    },
    {
      statName: "skwins",
      fullName: "Wins",
      type: "skater",
      catg: "general",
      subCatg: "General",
      subCatgSortOrder: 2,
      hidden: false
    },
    {
      statName: "skpassattempts",
      fullName: "Pass Attempts",
      type: "skater",
      catg: "offense",
      subCatg: "Passing",
      subCatgSortOrder: 1,
      hidden: false
    },
    {
      statName: "skpasses",
      fullName: "Pass Completions",
      type: "skater",
      catg: "offense",
      subCatg: "Passing",
      subCatgSortOrder: 2,
      hidden: false
    },
    {
      statName: "skpasspct",
      fullName: "Completion %",
      type: "skater",
      catg: "offense",
      subCatg: "Passing",
      subCatgSortOrder: 4,
      hidden: false
    },
    {
      statName: "sksaucerpasses",
      fullName: "Saucer Passes",
      type: "skater",
      catg: "offense",
      subCatg: "Passing",
      subCatgSortOrder: 5,
      hidden: false
    }
]

const populatePlayerData = (data, personalBests) => {
    let result = []
    let playerIds = personalBests.map(player => ({ name: player.name, id: player.playerId }))

    data.forEach(member => {
        let player = {}
        let stats = []
 
        player.name = member.name
        player.position = member.favoritePosition
        player.id = playerIds.find(player => player.name === member.name).id
        
        for ( const property in member ) {
            for ( let i = 0; i < dimensions.length; i++ ) {
                if ( dimensions[i].statName == property ) {
                                      
                    let stat = {
                        hidden: dimensions[i].hidden,            
                        statName: dimensions[i].statName,
                        fullName: dimensions[i].fullName,
                        type: dimensions[i].type,
                        catg: dimensions[i].catg,
                        subCatg: dimensions[i].subCatg,
                        subCatgSortOrder: dimensions[i].subCatgSortOrder,
                        value: member[property]}
                        
                    stats.push(stat)
                }
            }
        }

        player.stats = stats
        result.push(player)
    })
    return result
}

const updateCustomStats = (data) => {
    let result = data

    for ( let i = 0; i < result.length; i++ ) {
        let gamesPlayed = parseFloat(result[i].stats.find(stat => stat.statName === 'skgp').value)
        let goalsScored = parseFloat(result[i].stats.find(stat => stat.statName === 'skgoals').value)
        let assistsScored = parseFloat(result[i].stats.find(stat => stat.statName === 'skassists').value)
        let dekesMade = parseFloat(result[i].stats.find(stat => stat.statName === 'skdekesmade').value)
        let totalDekes = parseFloat(result[i].stats.find(stat => stat.statName === 'skdekes').value)
        let hitsMade = parseFloat(result[i].stats.find(stat => stat.statName === 'skhits').value)
        let totalFights = parseFloat(result[i].stats.find(stat => stat.statName === 'skfights').value)
        let trueTotalFights = parseFloat(result[i].stats.find(stat => stat.statName === 'skfights').value)
        let fightsWon = parseFloat(result[i].stats.find(stat => stat.statName === 'skfightswon').value)
        let totalGiveaways = parseFloat(result[i].stats.find(stat => stat.statName === 'skgiveaways').value)
        let totalOffsides = parseFloat(result[i].stats.find(stat => stat.statName === 'skoffsides').value)
        let totalPenMins = parseFloat(result[i].stats.find(stat => stat.statName === 'skpim').value)
        let totalPoints = parseFloat(result[i].stats.find(stat => stat.statName === 'skpoints').value)
        let totalShots = parseFloat(result[i].stats.find(stat => stat.statName === 'skshots').value)
        let totalTakeaways = parseFloat(result[i].stats.find(stat => stat.statName === 'sktakeaways').value)
        let deflections = parseFloat(result[i].stats.find(stat => stat.statName === 'skdeflections').value)
        let interceptions = parseFloat(result[i].stats.find(stat => stat.statName === 'skinterceptions').value)
        let passes = parseFloat(result[i].stats.find(stat => stat.statName === 'skpasses').value)
        
        if (totalFights === 0) {
            totalFights = 1
        }

        result[i].stats.push(
            {
                hidden: false
                ,catg: "offense"
                ,fullName: "Goals per Game"
                ,statName: "skgpg"
                ,subCatg: "Scoring"
                ,subCatgSortOrder: 4
                ,type: "skater"
                ,value: (gamesPlayed == 0 ? '0.0' : (goalsScored/gamesPlayed).toFixed(2).toString())
            }
        )

        result[i].stats.push(
            {
                hidden: false
                ,catg: "offense"
                ,fullName: "Assists per Game"
                ,statName: "skapg"
                ,subCatg: "Scoring"
                ,subCatgSortOrder: 5
                ,type: "skater"
                ,value: (gamesPlayed == 0 ? '0.0' : (assistsScored/gamesPlayed).toFixed(2).toString())
            }
        )

        result[i].stats.push(
            {
                hidden: false
                ,catg: "offense"
                ,fullName: "Deke Percentage"
                ,statName: "dekesmadepct"
                ,subCatg: "Skating"
                ,subCatgSortOrder: 4
                ,type: "skater"
                ,value: (totalDekes == 0 ? '0.0' : ((dekesMade/totalDekes)*100).toFixed(2).toString())
            }
        )

        result[i].stats.push(
            {
                hidden: false
                ,catg: "offense"
                ,fullName: "Successful Dekes per Game"
                ,statName: "dekesmadepg"
                ,subCatg: "Skating"
                ,subCatgSortOrder: 5
                ,type: "skater"
                ,value: (gamesPlayed == 0 ? '0.0' : (dekesMade/gamesPlayed).toFixed(2).toString())
            }
        )

        result[i].stats.push(
            {
                hidden: false
                ,catg: "defense"
                ,fullName: "Hits per Game"
                ,statName: "skhitspg"
                ,subCatg: "Goon Play"
                ,subCatgSortOrder: 2
                ,type: "skater"
                ,value: (gamesPlayed == 0 ? '0.0' : (hitsMade/gamesPlayed).toFixed(2).toString())
            }
        )

        result[i].stats.push(
            {
                hidden: false
                ,catg: "defense"
                ,fullName: "Fight Win Percentage"
                ,statName: "skfightwinpct"
                ,subCatg: "Goon Play"
                ,subCatgSortOrder: 5
                ,type: "skater"
                ,value: (totalFights == 0 ? '0.0' : ((fightsWon/totalFights)*100).toFixed(2).toString())
            }
        )

        result[i].stats.push(
            {
                hidden: false
                ,catg: "offense"
                ,fullName: "Giveaways per Game"
                ,statName: "skgiveawayspg"
                ,subCatg: "Bloopers"
                ,subCatgSortOrder: 3
                ,type: "skater"
                ,value: (gamesPlayed == 0 ? '0.0' : (totalGiveaways/gamesPlayed).toFixed(2).toString())
            }
        )

        result[i].stats.push(
            {
                hidden: false
                ,catg: "offense"
                ,fullName: "Offsides per Game"
                ,statName: "skoffsidespg"
                ,subCatg: "Bloopers"
                ,subCatgSortOrder: 5
                ,type: "skater"
                ,value: (gamesPlayed == 0 ? '0.0' : (totalOffsides/gamesPlayed).toFixed(2).toString())
            }
        )

        result[i].stats.push(
            {
                hidden: false
                ,catg: "defense"
                ,fullName: "PIM Excluding Fights (PIMEF)"
                ,statName: "skpimef"
                ,subCatg: "Goon Play"
                ,subCatgSortOrder: 7
                ,type: "skater"
                ,value: (totalPenMins - (trueTotalFights * 5)).toString()
            }
        )

        result[i].stats.push(
            {
                hidden: false
                ,catg: "defense"
                ,fullName: "PIMEF per Game"
                ,statName: "skpimefpg"
                ,subCatg: "Goon Play"
                ,subCatgSortOrder: 8
                ,type: "skater"
                ,value: (gamesPlayed == 0 ? '0.0' : ((totalPenMins - (trueTotalFights * 5))/gamesPlayed).toFixed(2).toString())
            }
        )

        result[i].stats.push(
            {
                hidden: false
                ,catg: "offense"
                ,fullName: "Points per Game"
                ,statName: "skpointspg"
                ,subCatg: "Scoring"
                ,subCatgSortOrder: 6
                ,type: "skater"
                ,value: (gamesPlayed == 0 ? '0.0' : (totalPoints/gamesPlayed).toFixed(2).toString())
            }
        )

        result[i].stats.push(
            {
                hidden: false
                ,catg: "offense"
                ,fullName: "Shots per Game"
                ,statName: "skshotspg"
                ,subCatg: "Shooting"
                ,subCatgSortOrder: 2
                ,type: "skater"
                ,value: (gamesPlayed == 0 ? '0.0' : (totalShots/gamesPlayed).toFixed(2).toString())
            }
        )

        result[i].stats.push(
            {
                hidden: false
                ,catg: "defense"
                ,fullName: "Takeaways per Game"
                ,statName: "sktakeawayspg"
                ,subCatg: "Defending"
                ,subCatgSortOrder: 2
                ,type: "skater"
                ,value: (gamesPlayed == 0 ? '0.0' : (totalTakeaways/gamesPlayed).toFixed(2).toString())
            }
        )

        result[i].stats.push(
            {
                hidden: false
                ,catg: "offense"
                ,fullName: "Shooting %"
                ,statName: "shotpct"
                ,subCatg: "Shooting"
                ,subCatgSortOrder: 5
                ,type: "skater"
                ,value: (totalShots == 0 ? '0.0' : ((goalsScored/totalShots)*100).toFixed(2).toString())
            }
        )

        result[i].stats.push(
            {
                hidden: false
                ,catg: "offense"
                ,fullName: "Deflections per Game"
                ,statName: "deflectionspg"
                ,subCatg: "Shooting"
                ,subCatgSortOrder: 4
                ,type: "skater"
                ,value: (gamesPlayed == 0 ? '0.0' : (deflections/gamesPlayed).toFixed(2).toString())
            }
        )

        result[i].stats.push(
            {
                hidden: false
                ,catg: "defense"
                ,fullName: "Interceptions per Game"
                ,statName: "skinterceptionspg"
                ,subCatg: "Defending"
                ,subCatgSortOrder: 5
                ,type: "skater"
                ,value: (gamesPlayed == 0 ? '0.0' : (interceptions/gamesPlayed).toFixed(2).toString())
            }
        )

        result[i].stats.push(
            {
                hidden: false
                ,catg: "offense"
                ,fullName: "Pass Completions per Game"
                ,statName: "skpassespg"
                ,subCatg: "Passing"
                ,subCatgSortOrder: 4
                ,type: "skater"
                ,value: (gamesPlayed == 0 ? '0.0' : (passes/gamesPlayed).toFixed(2).toString())
            }
        )


        result[i].playerNum = i
    }

    return result
}



const outputPlayerData = (playerData) => {
  fs.writeFile('player-data.json', JSON.stringify(updateCustomStats(populatePlayerData(playerData.members, personalBests)),null,2), 'utf8', (err) => {
    if (err) {
      console.log(err)}
    else {
      console.log("File written successfully\n")
    }
  })
}

outputPlayerData(playerData)