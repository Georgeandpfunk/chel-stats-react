const matchData = require('./match-history.json')
const fs = require('fs')
const clubId = '76815'

const uniqueifyMatchData = (data) => {
	return data.filter((v,i,a)=>a.findIndex(t=>(t.matchId === v.matchId))===i)
}

const playerStatArr = (matches) => {
  let result = []
  for (let i = 0; i < matches.length; i++ ) {
    const win = matches[i].clubs[`${clubId}`].goals > matches[i].clubs[`${Object.keys(matches[i].clubs).find(a => a !== clubId)}`].goals
    for (const [key, value] of Object.entries(matches[i].players[`${clubId}`])) {
      const matchStats = {
        name: value.playername,
        position: value.position,
        goals: parseInt(value.skgoals),
        assists: parseInt(value.skassists),
        hits: parseInt(value.skhits),
        faceoffWins: parseInt(value.skfow),
        faceoffLosses: parseInt(value.skfol),
        wins: win ? 1 : 0
      }
      result.push(matchStats)
    }
  }
  return result
}

const compilePlayerStats = (arr) => {
    const playerList = [...new Set(arr.map(player => player.name))]
    const arraySum = (arr, prop) => arr.map(x => x[`${prop}`]).reduce((a, b) => a + b, 0)
    let result = []

    for ( i = 0; i < playerList.length; i++ ) {
      allGames = arr.filter(player => player.name === playerList[i])
      centerGames = arr.filter(player => player.name === playerList[i] && player.position === 'center')
      wingGames = arr.filter(player => player.name === playerList[i] && (player.position === 'leftWing' || player.position === 'rightWing' ))
      defenseGames = arr.filter(player => player.name === playerList[i] && player.position === 'defenseMen')

      let playerObj = {
        name: playerList[i],
        all: {
          games: allGames.length.toString(),
          wins: arraySum(allGames,'wins').toString(),
          losses: (allGames.length - arraySum(allGames,'wins')).toString(),
          winPct: (parseFloat(allGames.length) === 0 ? '0.00%' : ((parseFloat(arraySum(allGames,'wins'))/parseFloat(allGames.length))*100).toFixed(2).toString() + '%'),
          goals: arraySum(allGames,'goals').toString(),
          assists: arraySum(allGames,'assists').toString(),
          hits: arraySum(allGames,'hits').toString(),
          goalsPg: (allGames.length === 0 ? '0.00' : (parseFloat(arraySum(allGames,'goals'))/parseFloat(allGames.length)).toFixed(2).toString()),
          assistsPg: (allGames.length === 0 ? '0.00' : (parseFloat(arraySum(allGames,'assists'))/parseFloat(allGames.length)).toFixed(2).toString()),
          pointsPg: (allGames.length === 0 ? '0.00' : ((parseFloat(arraySum(allGames,'assists'))+parseFloat(arraySum(allGames,'goals')))/parseFloat(allGames.length)).toFixed(2).toString()),
          hitsPg: (allGames.length === 0 ? '0.00' : (parseFloat(arraySum(allGames,'hits'))/parseFloat(allGames.length)).toFixed(2).toString()),
          faceoffPct: ((arraySum(allGames,'faceoffWins')+arraySum(allGames,'faceoffLosses')) === 0 ? '0.00%' : ((parseFloat(arraySum(allGames,'faceoffWins'))/parseFloat((arraySum(allGames,'faceoffWins')+arraySum(allGames,'faceoffLosses')))) * 100).toFixed(2).toString() + '%'),
        },
        center: {
          games: centerGames.length.toString(),
          wins: arraySum(centerGames,'wins').toString(),
          losses: (centerGames.length - arraySum(centerGames,'wins')).toString(),
          winPct: (parseFloat(centerGames.length) === 0 ? '0.00%' : (((parseFloat(arraySum(centerGames,'wins'))/parseFloat(centerGames.length))*100).toFixed(2).toString() + '%').replace('.00','')),
          goals: arraySum(centerGames,'goals').toString(),
          assists: arraySum(centerGames,'assists').toString(),
          hits: arraySum(centerGames,'hits').toString(),
          goalsPg: (centerGames.length === 0 ? '0.00' : (parseFloat(arraySum(centerGames,'goals'))/parseFloat(centerGames.length)).toFixed(2).toString()),
          assistsPg: (centerGames.length === 0 ? '0.00' : (parseFloat(arraySum(centerGames,'assists'))/parseFloat(centerGames.length)).toFixed(2).toString()),
          pointsPg: (centerGames.length === 0 ? '0.00' : ((parseFloat(arraySum(centerGames,'assists'))+parseFloat(arraySum(centerGames,'goals')))/parseFloat(centerGames.length)).toFixed(2).toString()),
          hitsPg: (centerGames.length === 0 ? '0.00' : (parseFloat(arraySum(centerGames,'hits'))/parseFloat(centerGames.length)).toFixed(2).toString()),
          faceoffPct: ((arraySum(centerGames,'faceoffWins')+arraySum(centerGames,'faceoffLosses')) === 0 ? '0.00%' : ((parseFloat(arraySum(centerGames,'faceoffWins'))/parseFloat((arraySum(centerGames,'faceoffWins')+arraySum(centerGames,'faceoffLosses')))) * 100).toFixed(2).toString() + '%'),
        },
        wing: {
          games: wingGames.length.toString(),
          wins: arraySum(wingGames,'wins').toString(),
          losses: (wingGames.length - arraySum(wingGames,'wins')).toString(),
          winPct: (parseFloat(wingGames.length) === 0 ? '0.00%' : (((parseFloat(arraySum(wingGames,'wins'))/parseFloat(wingGames.length))*100).toFixed(2).toString() + '%').replace('.00','')),
          goals: arraySum(wingGames,'goals').toString(),
          assists: arraySum(wingGames,'assists').toString(),
          hits: arraySum(wingGames,'hits').toString(),
          goalsPg: (wingGames.length === 0 ? '0.00' : (parseFloat(arraySum(wingGames,'goals'))/parseFloat(wingGames.length)).toFixed(2).toString()),
          assistsPg: (wingGames.length === 0 ? '0.00' : (parseFloat(arraySum(wingGames,'assists'))/parseFloat(wingGames.length)).toFixed(2).toString()),
          pointsPg: (wingGames.length === 0 ? '0.00' : ((parseFloat(arraySum(wingGames,'assists'))+parseFloat(arraySum(wingGames,'goals')))/parseFloat(wingGames.length)).toFixed(2).toString()),
          hitsPg: (wingGames.length === 0 ? '0.00' : (parseFloat(arraySum(wingGames,'hits'))/parseFloat(wingGames.length)).toFixed(2).toString()),
          faceoffPct: ((arraySum(wingGames,'faceoffWins')+arraySum(wingGames,'faceoffLosses')) === 0 ? '0.00%' : ((parseFloat(arraySum(wingGames,'faceoffWins'))/parseFloat((arraySum(wingGames,'faceoffWins')+arraySum(wingGames,'faceoffLosses')))) * 100).toFixed(2).toString() + '%'),
        },
        defense: {
          games: defenseGames.length.toString(),
          wins: arraySum(defenseGames,'wins').toString(),
          losses: (defenseGames.length - arraySum(defenseGames,'wins')).toString(),
          winPct: (parseFloat(defenseGames.length) === 0 ? '0.00%' : (((parseFloat(arraySum(defenseGames,'wins'))/parseFloat(defenseGames.length))*100).toFixed(2).toString() + '%').replace('.00','')),
          goals: arraySum(defenseGames,'goals').toString(),
          assists: arraySum(defenseGames,'assists').toString(),
          hits: arraySum(defenseGames,'hits').toString(),
          goalsPg: (defenseGames.length === 0 ? '0.00' : (parseFloat(arraySum(defenseGames,'goals'))/parseFloat(defenseGames.length)).toFixed(2).toString()),
          assistsPg: (defenseGames.length === 0 ? '0.00' : (parseFloat(arraySum(defenseGames,'assists'))/parseFloat(defenseGames.length)).toFixed(2).toString()),
          pointsPg: (defenseGames.length === 0 ? '0.00' : ((parseFloat(arraySum(defenseGames,'assists'))+parseFloat(arraySum(defenseGames,'goals')))/parseFloat(defenseGames.length)).toFixed(2).toString()),
          hitsPg: (defenseGames.length === 0 ? '0.00' : (parseFloat(arraySum(defenseGames,'hits'))/parseFloat(defenseGames.length)).toFixed(2).toString()),
          faceoffPct: ((arraySum(defenseGames,'faceoffWins')+arraySum(defenseGames,'faceoffLosses')) === 0 ? '0.00%' : ((parseFloat(arraySum(defenseGames,'faceoffWins'))/parseFloat((arraySum(defenseGames,'faceoffWins')+arraySum(defenseGames,'faceoffLosses')))) * 100).toFixed(2).toString() + '%'),
        },
      }
    result.push(playerObj)
    }
  console.log(result)
  return result
}


fs.writeFile('positionalData.json', JSON.stringify(compilePlayerStats(playerStatArr(uniqueifyMatchData(matchData))),null,2), 'utf8', (err) => {
  if (err) {
    console.log(err)}
  else {
    console.log("File written successfully\n")
  }
})