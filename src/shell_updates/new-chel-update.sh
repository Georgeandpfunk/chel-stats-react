rm -f temp.json
rm -f match-update.json
rm -f temp-seasonData.json
rm -f temp-seasonSummaryData.json
rm -f temp-player-data.json
rm -f positionalData.json
rm -f combinedBins.json
rm -f playerData.json
rm -f personal-bests.json

curl --referer www.ea.com "https://proclubs.ea.com/api/nhl/clubs/matches?clubIds=76815&platform=ps4&matchType=gameType5" -H "Accept-Language: en-US,en;q=0.5" -A "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:94.0) Gecko/20100101 Firefox/94.0" -H "Connection: keep-alive" -o temp.json
node outputMatchData.js
data=`cat match-update.json`

curl -v\
       -H "Content-Type: application/json" \
       -H "versioning: true" \
       -H "X-Master-key: \$2b\$10\$4b1UBV9gGUX9OULSEmbeqe.LfIBv1WbCDGL2ewwq2Kl9R1wt2xE06" \
       --request PUT \
       --data "@match-update.json" \
       https://api.jsonbin.io/v3/b/604287319d036f03499ec5a6


curl --referer www.ea.com "https://proclubs.ea.com/api/nhl/clubs/seasonalStats?clubIds=76815&platform=ps4" -H "Accept-Language: en-US,en;q=0.5" -A "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:94.0) Gecko/20100101 Firefox/94.0" -H "Connection: keep-alive" -o temp-seasonData.json
curl --referer www.ea.com "https://proclubs.ea.com/api/nhl/clubs/seasonRank?clubIds=76815&platform=ps4" -H "Accept-Language: en-US,en;q=0.5" -A "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:94.0) Gecko/20100101 Firefox/94.0" -H "Connection: keep-alive" -o temp-seasonSummaryData.json
curl --referer www.ea.com "https://proclubs.ea.com/api/nhl/members/stats?clubId=76815&platform=ps4" -H "Accept-Language: en-US,en;q=0.5" -A "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:94.0) Gecko/20100101 Firefox/94.0" -H "Connection: keep-alive"  -o temp-player-data.json

node outputPositionalData.js
node outputPersonalBests.js
node outputPlayerData.js

node combineAllBins.js

data=`cat combinedBins.json`

curl -v\
       -H "Content-Type: application/json" \
       -H "versioning: true" \
       -H "X-Master-key: \$2b\$10\$4b1UBV9gGUX9OULSEmbeqe.LfIBv1WbCDGL2ewwq2Kl9R1wt2xE06" \
       --request PUT \
       --data "@combinedBins.json" \
       https://api.jsonbin.io/v3/b/61b917b80ddbee6f8b1dbf9e

rm -f temp.json
rm -f temp-seasonData.json
rm -f temp-seasonSummaryData.json
rm -f temp-player-data.json
rm -f positionalData.json
rm -f combinedBins.json
rm -f playerData.json
rm -f personal-bests.json