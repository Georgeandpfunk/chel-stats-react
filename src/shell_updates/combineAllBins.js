const seasonData = require('./temp-seasonData.json')
const settingsData = require('./settings.json')
const seasonSummaryData = require('./temp-seasonSummaryData.json')
const positionalData = require('./positionalData.json')
const playerData = require('./player-data.json')
const personalBests = require('./personal-bests.json')

const fs = require('fs')

const combineAllBins = () => {
  return {
    seasonData: seasonData,
    settingsData: settingsData,
    seasonSummaryData: seasonSummaryData,
    positionalData: positionalData,
    playerData: playerData,
    personalBests: personalBests
  }
}

fs.writeFile('combinedBins.json', JSON.stringify(combineAllBins(),null,2), 'utf8', (err) => {
  if (err) {
    console.log(err)}
  else {
    console.log("File written successfully\n")
  }
})