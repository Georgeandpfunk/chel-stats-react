//import data from '../helpers/data.js'

const matchData = require('./match-history.json')
//const dimensions = data.bestsDimensions
const clubId = '76815'
const fs = require('fs')

const dimensions = [
	{
		id: 1,
		statName: 'goals',
		displayName: 'Goals',
		cardNamePlural: ' goals',
		cardNameSingular: ' goals',
		sortOrder: 1,
	},
	{
		id: 2,
		statName: 'assists',
		displayName: 'Assists',
		cardNamePlural: ' assists',
		cardNameSingular: ' assist',
		sortOrder: 2,
	},
	{
		id: 3,
		statName: 'points',
		displayName: 'Points',
		cardNamePlural: ' points',
		cardNameSingular: ' point',
		sortOrder: 3,
	},
	{
		id: 4,
		statName: 'faceoffWinPct',
		displayName: 'Faceoff Win Rate',
		cardNamePlural: '% win rate',
		cardNameSingular: '% win rate',
		sortOrder: 4,
	},
	{
		id: 5,
		statName: 'passingPct',
		displayName: 'Passing Completion Rate',
		cardNamePlural: '% completion rate',
		cardNameSingular: '% completion rate',		
		sortOrder: 5,
	},
	{
		id: 6,
		statName: 'hits',
		displayName: 'Hits',
		cardNamePlural: ' hits',
		cardNameSingular: ' hit',
		sortOrder: 6,
	},
	{
		id: 7,
		statName: 'interceptions',
		displayName: 'Interceptions',
		cardNamePlural: 'interceptions',
		cardNameSingular: 'interception',
		sortOrder: 7,
	},
	{
		id: 8,
		statName: 'blockedShots',
		displayName: 'Blocked Shots',
		cardNamePlural: ' blocked shots',
		cardNameSingular: ' blocked shot',
		sortOrder: 8,
	},
	{
		id: 9,
		statName: 'takeaways',
		displayName: 'Takeaways',
		cardNamePlural: ' takeaways',
		cardNameSingular: ' takeaway',
		sortOrder: 9,
	},
]

const uniqueifyMatchData = (data) => {
	return data.filter((v,i,a)=>a.findIndex(t=>(t.matchId === v.matchId))===i)
}

const outputPersonalBests = (data) => {
	const playerList = [...new Set(data.map(player => player.name))]
	const statList = [... new Set(dimensions.map(stat => stat.statName))]
	const findMaxStatValue = (arr, statName) => Math.max.apply(Math, arr.map(o => o[`${statName}`]))

	let result = []

	for ( i = 0; i < playerList.length; i++ ) {
		let arr = data.filter(match => match.name === playerList[i])
		let player = { name: playerList[i], playerId: arr[0].playerId }
		for ( j = 0; j < statList.length; j++ ) {
			statName = statList[j]
			statMax = findMaxStatValue(arr,statName)
			player[`${statName}`] = { max: statMax, matches: arr.filter(match => match[`${statName}`] == statMax).map(match => match.matchId) }
		}
		result.push(player)
	}

  return result
}

const playerStatArr = (matches) => {
  let result = []
  for (let i = 0; i < matches.length; i++ ) {
    const win = matches[i].clubs[`${clubId}`].goals > matches[i].clubs[`${Object.keys(matches[i].clubs).find(a => a !== clubId)}`].goals
    for (const [key, value] of Object.entries(matches[i].players[`${clubId}`])) {
      const matchStats = {
        name: value.playername,
        matchId: matches[i].matchId,
        playerId: key,
        position: value.position,
        goals: parseInt(value.skgoals),
        assists: parseInt(value.skassists),
        points: parseInt(value.skassists) + parseInt(value.skgoals),
        hits: parseInt(value.skhits),
        interceptions: parseInt(value.skinterceptions),
        blockedShots: parseInt(value.skbs),
        takeaways: parseInt(value.sktakeaways),
        passingPct: parseFloat(value.skpasspct),
        faceoffWinPct: parseInt(value.skfol)+parseInt(value.skfow) === 0 ? 0.00 : ((parseFloat(value.skfow)/(parseInt(value.skfol)+parseFloat(value.skfow)))*100).toFixed(2),
        wins: win ? 1 : 0
      }
      result.push(matchStats)
    }
  }
  return result
}

const outputFile = (matchData) => {
  fs.writeFile('personal-bests.json', JSON.stringify(outputPersonalBests(playerStatArr(uniqueifyMatchData(matchData))),null,2), 'utf8', (err) => {
    if (err) {
      console.log(err)}
    else {
      console.log("File written successfully\n")
    }
  })
}

outputFile(matchData)