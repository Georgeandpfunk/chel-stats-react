const { MongoClient } = require("mongodb");

const uri = "mongodb+srv://fullstack:seanheck@cluster0.ltofs.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"

//get players
MongoClient.connect(uri, function(err, db) {
  if (err) throw err;
  var dbo = db.db("chel");
  dbo.collection("players").findOne({}, function(err, result) {
    if (err) throw err;
    console.log(result);
    db.close();
  });
}); 

//get matches
MongoClient.connect(uri, function(err, db) {
  if (err) throw err;
  var dbo = db.db("chel");
  dbo.collection("matches").find({}).toArray(function(err, result) {
    if (err) throw err;
      console.log(result);
    db.close();
  });
});

//get settings
MongoClient.connect(uri, function(err, db) {
  if (err) throw err;
  var dbo = db.db("chel");
  dbo.collection("appSettings").findOne({}, function(err, result) {
    if (err) throw err;
    console.log(result);
    db.close();
  });
});

//get stats
MongoClient.connect(uri, function(err, db) {
  if (err) throw err;
  var dbo = db.db("chel");
  dbo.collection("seasonStatistics").findOne({}, function(err, result) {
    if (err) throw err;
    console.log(result);
    db.close();
  });
});

//get summary
MongoClient.connect(uri, function(err, db) {
  if (err) throw err;
  var dbo = db.db("chel");
  dbo.collection("seasonSummary").findOne({}, function(err, result) {
    if (err) throw err;
    console.log(result);
    db.close();
  });
}); 